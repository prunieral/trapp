var express = require('express');
var app = express();
var path = require('path');
var React = require('react');
var ReactDOMServer = require('react-dom/server');
var fs = require('fs');
var spawn = require('child_process').spawn;
var axios = require('axios');
var bodyParser = require('body-parser');
var sleep = require('system-sleep');
var fetch = require('node-fetch');
var fs = require('fs');
var readline = require('readline');

app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static(path.join(__dirname, '..', 'data')))
app.use(bodyParser.json());

var html = fs.readFileSync(path.join(__dirname, 'public/index.html'));
var port = 8001

function getPythonCommand() {
	if (process.platform === "win32") {
	    return "py";
	} else {
	    return "python3.6";
	}
}

app.set('nodes', [])

app.get('/', function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});    res.end(html);
})

app.get('/getStockExchanges', function(req, res) {
    var pth = path.join(__dirname, '../data/');
    var dirs = p => fs.readdirSync(p).filter(f => !fs.statSync(path.join(p, f)).isDirectory() && f != 'DATABASE.json');
    var stockExchanges = dirs(pth);
    var i;
    for (i=0; i<stockExchanges.length; i++) {
        stockExchanges[i] = stockExchanges[i].split('.')[0]
    }
    res.json(stockExchanges);
})

app.get('/addStockExchange', function(req, res) {
    var filePath = path.join(__dirname, '../data/', req.query.stockExchange + '.txt');
    var dirPath = path.join(__dirname, '../data/', req.query.stockExchange);
    if (!fs.existsSync(filePath)){
        fs.writeFile(filePath, "", function(err) {
            if(err) {
                console.log(err);
            } else {
                console.log("The file was saved!");
            }
        });
    }
    if (!fs.existsSync(dirPath)){
        fs.mkdirSync(dirPath);
    }
    res.json({});
})

app.get('/deleteStockExchange', function(req, res) {
    var filePath = path.join(__dirname, '../data/', req.query.stockExchange + '.txt');
    var dirPath = path.join(__dirname, '../data/', req.query.stockExchange);
    if (fs.existsSync(filePath)) {
        fs.unlinkSync(filePath);
    }
    if (fs.existsSync(dirPath)) {
        fs.readdir(dirPath, (err, files) => {
          files.forEach(file => {
            fs.unlinkSync(path.join(__dirname, '../data/', req.query.stockExchange, file));
          });
        })
        fs.rmdirSync(dirPath);
    }
    res.json({});
})

app.get('/getSecurities', function(req, res) {
    fs.readdir(path.join(__dirname, '../data/' + req.query.stockexchange), function (err, files) {
        if (err) {
            throw err;
        }
        files = files.map(function x(value) {return value.split(".")[0]});
        res.json(files);
    });
})

app.get('/getSecurityFilesInfo', function(req, res) {
    var fileInfo = []

    var rd = readline.createInterface({
        input: fs.createReadStream(path.join(__dirname, '../data/', req.query.stockexchange + '.txt')),
        output: process.stdout,
        console: false
    });

    rd.on('line', function(line) {
        if (line.length > 0) {
            var filePath = path.join(__dirname, '../data/', req.query.stockexchange, line + '.csv')
            if (!fs.existsSync(filePath)) {
                fileInfo.push({ id : line, lastRefresh : 'No data' });
            } else {
                var stats = fs.statSync(filePath);
                var mtime = stats.mtime.toLocaleString()
                fileInfo.push({ id : line, lastRefresh : mtime });
            }
        }
    });
    rd.on('close', function (line) {
        res.json(fileInfo);
    });
})

app.get('/addSecurity', function(req, res) {
    var targetedFile = path.join(__dirname, '../data/' + req.query.stockExchange + '.txt')
    fs.appendFile(targetedFile, '\n' + req.query.stock, 'utf8', function (err) {
        if (err) {
            console.log(err);
            res.json({error : err});
        } else {
            res.json({result : 'OK'});
        }
    });
})

app.get('/deleteSecurities', function(req, res) {
    var targetedFile = path.join(__dirname, '../data/' + req.query.stockExchange + '.txt')

    if (req.query.stockExchange != 'null') {
        var stocks = req.query.stocks.split(",");

        fs.readFile(targetedFile, 'utf-8', function(err, content){
            if (err) res.json({error : err});

            var newContent = "";
            var lines = content.split('\n');

            for (var i = 0; i < lines.length; i++) {
                if (stocks.indexOf(lines[i].trim()) == -1 && lines[i].length > 0) {
                    newContent += lines[i].trim() + '\n';
                }
            }

            fs.writeFile(targetedFile, newContent, 'utf-8', function (err) {
                if (err) res.json({error : err});
                res.json({result : 'OK'});
            });
        });

    } else {
        res.json({error : 'null value found as stock exchange'});
    }
})

app.get('/refreshSecurities', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/AppDownloadSecuritiesData.py')];
    var process = spawn(getPythonCommand(), script);
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).send(data);
    });
    process.stderr.on('data', function(data) {
        console.log(data);
        return res.status(500).send(data);
    });
})

app.get('/getForecasts', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/AppForecaster.py')];
    var params = [req.query.ste,req.query.sec,req.query.startDate,
                  req.query.endDate,req.query.ratio,req.query.days,
                  req.query.knn]
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).send(data);
    });
    process.stderr.on('data', function(data) {
        console.log(data);
        return res.status(500).send(data);
    });
})

app.get('/getAssessments', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/GetAssessments.py')];
    var params = []
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).end(data);
    });
    process.stderr.on('data', function(data) {
        return res.status(500).end(data);
    });
})

app.get('/getAssessmentDetails', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    var script = [path.join(__dirname, '../python/web/GetAssessmentDetails.py')];
    var params = [req.query.assessment]
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).end(data);
    });
    process.stderr.on('data', function(data) {
        return res.status(500).end(data);
    });
})

app.get('/getSchedule', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/GetSchedule.py')];
    var params = []
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).send(data);
    });
    process.stderr.on('data', function(data) {
        console.log(data);
        return res.status(500).send(data);
    });
})

app.listen(port, function() {
    console.log('Node.js web server at port ' + port + ' is running...')
})