import { combineReducers } from 'redux';

import globalReducer from './GlobalReducer.js';
import homeReducer from './HomeReducer.js';
import consultReducer from './ConsultReducer.js';
import dashboardReducer from './DashboardReducer.js';
import backtestReducer from './BacktestReducer.js';
import assessmentReducer from './AssessmentReducer.js';
import settingsReducer from './SettingsReducer.js';
import timetableReducer from './TimetableReducer.js';

export default combineReducers({

    global : globalReducer,
    pagehome : homeReducer,
    pageconsult : consultReducer,
    pagedashboard : dashboardReducer,
    pagebacktest : backtestReducer,
    pageassessment : assessmentReducer,
    pagesettings : settingsReducer,
    pagetimetable : timetableReducer,

})