

const initialState = {
    nextID: 1,
    layout: [],
    openDialog: false,
    stockExchanges: [],
    securities: [],
    stockExchange: '',
    security: '',
    selectedElement: '',
}

export default function dashboardReducer(state=initialState, action) {

    switch (action.type){
        case "DASHBOARD_ADD_ELEMENT": {
            state.layout = state.layout.concat({i: 'el' + state.nextID,
                                x: 0,
                                y: Infinity,
                                w: 6,
                                h: 4,
                                minW: 4,
                                minH: 3,
                                width: 180,
                                height: 250,
                                draggableHandle: ".draggable",
                                interval:"D",
                                stockExchange: '',
                                security: '',
                                data: []});
            state.nextID = state.nextID + 1;
            break;
        }
        case "DASHBOARD_REMOVE_ELEMENT": {
            state.layout = state.layout.filter(obj => action.payload != obj.i);
            break;
        }
        case "DASHBOARD_REMOVE_ALL_ELEMENTS": {
            state.layout = [];
            break;
        }
        case "DASHBOARD_CONFIGURATE_ELEMENT": {
            state.openDialog = true;
            state.selectedElement = action.payload;
            break;
        }
        case "DASHBOARD_SAVE_CONFIGURATION": {
            state.openDialog = false;
            break;
        }
        case "DASHBOARD_SAVE_STATE": {
            var newLayout = [];
            for (var el in action.payload) {
                newLayout.push( { ...state.layout.filter(obj => obj.i == action.payload[el].i)[0] ,
                                 h : action.payload[el].h,
                                 w : action.payload[el].w,
                                 y : action.payload[el].y,
                                 x : action.payload[el].x,
                                 width : (document.getElementsByClassName("chart-"+action.payload[el].i).item(0).clientWidth)-15,
                                 height : (document.getElementsByClassName("chart-"+action.payload[el].i).item(0).clientHeight)-56})
            }
            state.layout = newLayout;
            break;
        }
        case "DASHBOARD_SET_INTERVAL": {
            console.log("VALUE : " + action.payload.value);
            for (var el in state.layout) {
                console.log(state.layout[el].i);
                console.log(action.payload.id);
                if (state.layout[el].i == action.payload.id) {
                    state.layout[el].interval = action.payload.value;
                }
            }
            break;
        }
        case "DASHBOARD_SET_STOCKEXCHANGES": {
            state = {...state, stockExchanges:action.payload}
            break;
        }
        case "DASHBOARD_SET_SECURITIES": {
            state = {...state, securities:action.payload}
            break;
        }
        case "DASHBOARD_CHANGE_STOCKEXCHANGE": {
            state = {...state, stockExchange:action.payload}
            break;
        }
        case "DASHBOARD_CHANGE_SECURITY": {
            state = {...state, security:action.payload}
            break;
        }
        case "DASHBOARD_CANCEL_DIALOG": {
            state = {...state, openDialog: false, stockExchange: '', security: ''}
            break;
        }
        case "DASHBOARD_VALIDATE_DIALOG": {
            state = {...state, openDialog: false, stockExchange: '', security: ''}
            break;
        }
        case "DASHBOARD_SET_DATA": {
            for (var el in state.layout) {
                if (state.layout[el].i == state.selectedElement) {
                    state.layout[el].data = action.payload;
                }
            }
            break;
        }
        case "DASHBOARD_SET_INFO_OF_DATA": {
            for (var el in state.layout) {
                if (state.layout[el].i == state.selectedElement) {
                    state.layout[el].stockExchange = action.payload.stockExchange;
                    state.layout[el].security = action.payload.security;
                }
            }
            break;
        }
    }

    return state;
}