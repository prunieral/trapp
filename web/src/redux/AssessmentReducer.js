

const initialState = {
    selectedAssessment: "",
    assessments: [],
    assessmentDetail: [],
    layout: [{i: '1',
                x: 0,
                y: Infinity,
                w: 6,
                h: 4,
                minW: 4,
                minH: 3,
                width: 180,
                height: 250,
                draggableHandle: ".draggable"}],
}

export default function assessmentReducer(state=initialState, action) {

    switch (action.type){
        case "ASSESSMENT_SET_ASSESSMENTS": {
            state = {...state, assessments: action.payload}
            break;
        }
        case "ASSESSMENT_SET_ASSESSMENT_DETAIL": {
            state = {...state, assessmentDetail: action.payload}
            break;
        }
        case "ASSESSMENT_SET_SELECTED_ASSESSMENT": {
            state = {...state, selectedAssessment: action.payload}
            break;
        }
        case "ASSESSMENT_SET_ASSESSMENT_DETAIL": {
            state = {...state, assessments: action.payload}
            break;
        }
        case "ASSESSMENT_SAVE_STATE": {
            var newLayout = [];
            for (var el in action.payload) {
                newLayout.push( { ...state.layout.filter(obj => obj.i == action.payload[el].i)[0] ,
                                 h : action.payload[el].h,
                                 w : action.payload[el].w,
                                 y : action.payload[el].y,
                                 x : action.payload[el].x,
                                 width : (document.getElementsByClassName("chart-"+action.payload[el].i).item(0).clientWidth)-15,
                                 height : (document.getElementsByClassName("chart-"+action.payload[el].i).item(0).clientHeight)-56})
            }
            state.layout = newLayout;
            break;
        }
    }

    return state;
}