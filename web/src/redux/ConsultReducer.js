import { contains } from "../utils";

const initialState = {
    stockExchanges : [],
    securities : [],
    stockExchange : "",
    security : "",
    data : []
}

export default function consultReducer(state=initialState, action) {

    switch (action.type){
        case "SET_STOCKEXCHANGES": {
            state = {...state, stockExchanges:action.payload}
            break;
        }
        case "SET_SECURITIES": {
            console.log(action.payload);
            state = {...state, securities:action.payload, security:""}
            break;
        }
        case "SET_DATA": {
            state = {...state, data: action.payload}
            break;
        }
        case "CHANGE_STOCKEXCHANGE": {
            state = {...state, stockExchange:action.payload}
            break;
        }
        case "CHANGE_SECURITY": {
            state = {...state, security:action.payload}
            break;
        }
    }

    return state;
}