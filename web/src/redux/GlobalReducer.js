
const initialState = {
    stockExchanges : [],
    activePage : "home",
    size: {},
    cookies: {},
    isSaveSnackbarOpen: false,
    saveMode: false,
};

export default function globalReducer(state=initialState, action) {

    switch (action.type){
        case "SET_SAVE_STATE": {
            state = {...state, saveMode : action.payload }
            break;
        }
        case "SET_APP_STATE": {
            state = action.payload;
            break;
        }
        case "SET_COOKIE_HANDLER": {
            state = {...state, cookies : action.payload }
            break;
        }
        case "SET_SIZE": {
            state = {...state, size : action.payload }
            break;
        }
        case "SET_HOME_PAGE": {
            state = {...state, activePage : 'home' }
            break;
        }
        case "SET_CONSULTATION_PAGE": {
            state = {...state, activePage : 'consultation' }
            break;
        }
        case "SET_DASHBOARD_PAGE": {
            state = {...state, activePage : 'dashboard' }
            break;
        }
        case "SET_ASSESSMENT_PAGE": {
            state = {...state, activePage : 'assessment' }
            break;
        }
        case "SET_BACKTEST_PAGE": {
            state = {...state, activePage : 'backtest' }
            break;
        }
        case "SET_SETTINGS_PAGE": {
            state = {...state, activePage : 'settings' }
            break;
        }
        case "SET_TIMETABLE_PAGE": {
            state = {...state, activePage : 'timetable' }
            break;
        }
        case "GL_SET_STOCKEXCHANGES": {
            state = {...state, stockExchanges : action.payload}
            break;
        }
        case "SET_SAVE_SNACKBAR_STATE": {
            state = {...state, isSaveSnackbarOpen : action.payload}
            break;
        }
    }

    return state;
}