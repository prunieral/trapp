

const initialState = {
    selectedModel: {},
    capital: 10000,
    startDate: new Date(),
    minComission: 4,
    percentComission: .1,
}

export default function backtestReducer(state=initialState, action) {

    switch (action.type){
        case "BACKTEST_SET_MODEL": {
            state = {...state, selectedModel: action.payload};

            break;
        }
        case "BACKTEST_SET_FIELD_VALUE": {
            for (var attr in action.payload) { state[attr] = action.payload[attr]; }
            console.log(state);
            break;
        }
    }

    return state;
}