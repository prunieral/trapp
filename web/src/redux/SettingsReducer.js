

const initialState = {
    stockExchanges : [],
    selectedStockExchange : null,
    stocks : [],
    selectedStocks : [],
    isStockExchangesLoading : false,
    addDialogFieldValue : '',
    addDialogStockFieldValue : '',
    isRemoveDialogOpen : false,
    isAddDialogOpen : false,
    isAddStockDialogOpen : false,
    isSnackbarOpen : false,
    isNoSelectionSnackbarOpen : false,
    isNoExchangeSelectionSnackbarOpen : false,
}

export default function settingsReducer(state=initialState, action) {

    switch (action.type){
        case "SETTINGS_SET_STOCKEXCHANGES": {
            state = {...state, stockExchanges: action.payload}
            break;
        }
        case "SETTINGS_SET_STOCKEXCHANGE": {
            state = {...state, selectedStockExchange: action.payload}
            break;
        }
        case "SETTINGS_SET_REMOVE_DIALOG_STATE": {
            state = {...state, isRemoveDialogOpen: action.payload}
            break;
        }
        case "SETTINGS_SET_ADD_DIALOG_STATE": {
            state = {...state, isAddDialogOpen: action.payload}
            break;
        }
        case "SETTINGS_SET_ADD_STOCK_DIALOG_STATE": {
            state = {...state, isAddStockDialogOpen: action.payload}
            break;
        }
        case "SETTINGS_SET_ADD_FIELD_VALUE": {
            state = {...state, addDialogFieldValue: action.payload}
            break;
        }
        case "SETTINGS_SET_ADD_STOCK_FIELD_VALUE": {
            state = {...state, addDialogStockFieldValue: action.payload}
            break;
        }
        case "SETTINGS_SET_SNACKBAR_STATE": {
            state = {...state, isSnackbarOpen: action.payload}
            break;
        }
        case "SETTINGS_SET_NO_SELECTION_SNACKBAR_STATE": {
            state = {...state, isNoSelectionSnackbarOpen: action.payload}
            break;
        }
        case "SETTINGS_SET_NO_EXCHANGE_SNACKBAR_STATE": {
            state = {...state, isNoExchangeSelectionSnackbarOpen: action.payload}
            break;
        }
        case "SETTINGS_SET_STOCKEXCHANGES_LOADING": {
            state = {...state, isStockExchangesLoading: action.payload}
            break;
        }
        case "SETTINGS_SET_STOCKS_INFO": {
            state = {...state, stocks: action.payload}
            break;
        }
        case "SETTINGS_SET_SELECTED_STOCKS": {
            state = {...state, selectedStocks: action.payload}
            break;
        }
    }

    return state;
}