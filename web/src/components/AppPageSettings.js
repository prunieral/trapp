import React, {Component}  from 'react';
import ReactDOM from 'react-dom';
import AppCandleStickChart from './AppCandleStickChart.js';
import { getData, contains } from "../utils.js";
import { TypeChooser } from "react-stockcharts/lib/helper";
import Button from '@material-ui/core/Button';
import { connect } from "react-redux";
import axios from 'axios';
import Chip from '@material-ui/core/Chip';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import AppLoading from './AppLoading.js';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import ContainerDimensions from 'react-container-dimensions';
import EnhancedTable from './AppTable.js';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';

@connect((store) => {
    return {
        settings : store.pagesettings,
        global : store.global
    }
})
export default class AppPageSettings extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    refreshStockExchanges() {
        this.props.dispatch({type: "SETTINGS_SET_STOCKEXCHANGES_LOADING", payload: true});
        axios.get('getStockExchanges').then(response => {
            this.props.dispatch({type: "SETTINGS_SET_STOCKEXCHANGES", payload: response.data});
            this.props.dispatch({type: "SETTINGS_SET_STOCKEXCHANGES_LOADING", payload: false});
            this.setState({});
        }).catch((error) => {
            console.log(error);
        });
    }

	handleRequestDelete(key) {
	    this.props.dispatch({type: "SETTINGS_SET_STOCKEXCHANGE", payload: key});
	    this.props.dispatch({type: "SETTINGS_SET_REMOVE_DIALOG_STATE", payload: true});
    };

    handleClickChip(key) {
        this.props.dispatch({type: "SETTINGS_SET_STOCKEXCHANGE", payload: key});
        axios.get('getSecurityFilesInfo?stockexchange=' + key).then(response => {
            this.props.dispatch({type: "SETTINGS_SET_STOCKS_INFO", payload: response.data});
        }).catch((error) => {
            console.log(error);
        });
    };

    handleDialogCancelClose() {
        this.props.dispatch({type: "SETTINGS_SET_REMOVE_DIALOG_STATE", payload: false});
    };

    handleDialogClose() {
        axios.get('deleteStockExchange?stockExchange=' + this.props.settings.selectedStockExchange).then(response => {
            this.refreshStockExchanges();
        }).catch((error) => {
            console.log(error);
        });

        this.handleDialogCancelClose();
    };

    handleAddDialogCancelClose() {
        this.props.dispatch({type: "SETTINGS_SET_ADD_DIALOG_STATE", payload: false});
    };

    handleAddStockDialogCancelClose() {
        this.props.dispatch({type: "SETTINGS_SET_ADD_STOCK_DIALOG_STATE", payload: false});
    };

    handleClickAddChip() {
        this.props.dispatch({type: "SETTINGS_SET_ADD_DIALOG_STATE", payload: true});
    };

    handleAddDialogClose() {
        if (this.props.settings.addDialogFieldValue.length > 2) {
            axios.get('addStockExchange?stockExchange=' + this.props.settings.addDialogFieldValue).then(response => {
                this.refreshStockExchanges();
                this.props.dispatch({type: "SETTINGS_SET_ADD_DIALOG_STATE", payload: false});
                this.props.dispatch({type: "SETTINGS_SET_ADD_FIELD_VALUE", payload: ''});
            }).catch((error) => {
                console.log(error);
            });
        } else {
            this.props.dispatch({type: "SETTINGS_SET_SNACKBAR_STATE", payload: true});
        }
    };

    handleAddStockDialogClose() {
        if (this.props.settings.addDialogStockFieldValue.length > 2) {
            axios.get('addSecurity' +
                        '?stockExchange=' + this.props.settings.selectedStockExchange +
                        '&stock=' + this.props.settings.addDialogStockFieldValue).then(response => {
                this.props.dispatch({type: "SETTINGS_SET_ADD_STOCK_DIALOG_STATE", payload: false});
                this.props.dispatch({type: "SETTINGS_SET_ADD_STOCK_FIELD_VALUE", payload: ''});
                axios.get('getSecurityFilesInfo?stockexchange=' + this.props.settings.selectedStockExchange).then(response => {
                    this.props.dispatch({type: "SETTINGS_SET_STOCKS_INFO", payload: response.data});
                    this.handleClickChip(this.props.settings.selectedStockExchange);
                }).catch((error) => {
                    console.log(error);
                });
            }).catch((error) => {
                console.log(error);
            });
        } else {
            this.props.dispatch({type: "SETTINGS_SET_SNACKBAR_STATE", payload: true});
        }
    };

    handleTableRowSelection(selectedRows) {
        this.props.dispatch({type: "SETTINGS_SET_SELECTED_STOCKS", payload: selectedRows.slice(0)});
    }

    handleAddStock() {
        if (this.props.settings.selectedStockExchange != null) {
            this.props.dispatch({type: "SETTINGS_SET_ADD_STOCK_DIALOG_STATE", payload: true});
        } else {
            this.props.dispatch({type: "SETTINGS_SET_NO_EXCHANGE_SNACKBAR_STATE", payload: true});
        }
    };

    handleDeleteStock(stocks) {
        var text = '';

        stocks.map((stock) => {
            text += ',' + stock
        });

        if (text.length > 0)
            text = text.substr(1);

        this.props.settings.selectedStocks = text;

        if (this.props.settings.selectedStocks.length > 0) {
            axios.get('deleteSecurities' +
                        '?stockExchange=' + this.props.settings.selectedStockExchange +
                        '&stocks=' + this.props.settings.selectedStocks).then(response => {
                axios.get('getSecurityFilesInfo?stockexchange=' + this.props.settings.selectedStockExchange).then(response => {
                    this.props.dispatch({type: "SETTINGS_SET_STOCKS_INFO", payload: response.data});
                }).catch((error) => {
                    console.log(error);
                });
            }).catch((error) => {
                console.log(error);
            });
        } else {
            this.props.dispatch({type: "SETTINGS_SET_NO_SELECTION_SNACKBAR_STATE", payload: true});
        }
    };

    handleTextFieldChange(event) {
        this.props.dispatch({type: "SETTINGS_SET_ADD_FIELD_VALUE", payload: event.target.value});
    };

    handleStockFieldChange(event) {
        this.props.dispatch({type: "SETTINGS_SET_ADD_STOCK_FIELD_VALUE", payload: event.target.value});
    };

    handleSnackbarClose() {
        this.props.dispatch({type: "SETTINGS_SET_SNACKBAR_STATE", payload: false});
    }

    handleNoSelectionSnackbarClose() {
        this.props.dispatch({type: "SETTINGS_SET_NO_SELECTION_SNACKBAR_STATE", payload: false});
    }

    handleNoExchangeSelectionSnackbarClose() {
        this.props.dispatch({type: "SETTINGS_SET_NO_EXCHANGE_SNACKBAR_STATE", payload: false});
    }

	renderChip(data) {
        return (
          <Chip
            key={data}
            label={data}
            onDelete={this.handleRequestDelete.bind(this, data)}
            onClick={this.handleClickChip.bind(this, data)}
            style={{margin: 4}}
          />
        );
      }

    componentDidMount() {
        this.refreshStockExchanges();
	}

    render() {

        var styles = {
          wrapper: {
            display: 'flex',
            flexWrap: 'wrap',
          },
          loading: {
            width: '100%',
            height: 30,
            marginTop: 10,
            marginBottom: 10,
          },
          formControl: {
                minWidth: 200,
          },
          center: {
                justifyContent: 'center',
                alignItems: 'center',
                textAlign: 'center'
          }
        };

        if(this.props.settings.isStockExchangesLoading == false) {
            var StockExchangesChips =
                <div style={{...styles.wrapper,...styles.center}}>
                    {this.props.settings.stockExchanges.map(this.renderChip, this)}
                </div>;
        } else {
            var StockExchangesChips =
                <div style={styles.loading}>
                    <AppLoading />
                </div>;
        }

		return (
		<div className="page">
		    <div className="title-page">Settings</div>
		    <div id="pageConsult-Header">
		        <Grid container
		            alignItems="center"
		            justify="center">
		            <Grid item xs={12} sm={10} md={8} lg={8}>
                        <Grid container
                            justify="center"
                            alignItems="center"
                            spacing={32}>
                            <Grid item
                                xs={12}>
                                <div className="subtitle-page">Stock exchange management</div>
                                {StockExchangesChips}
                                <div
                                    style={styles.center}>
                                    <Button
                                        variant="fab"
                                        color="primary"
                                        aria-label="Add"
                                        onClick={this.handleClickAddChip.bind(this)}
                                        style={{ transform : 'scale(0.6)' }}>
                                        <AddIcon />
                                      </Button>
                                </div>
                            </Grid>

                            <Grid item
                                xs={12}>
                                <div className="subtitle-page">Stock management</div>
                                <div>
                                <ContainerDimensions>
                                { ({ width, height }) =>
                                    <EnhancedTable
                                        name={this.props.settings.selectedStockExchange == null ? "" :
                                              this.props.settings.selectedStockExchange + "'s Stocks"}
                                        data={this.props.settings.stocks}
                                        width={width}
                                        onAdd={this.handleAddStock.bind(this)}
                                        onDelete={this.handleDeleteStock.bind(this)}
                                      />
                                }
                                </ContainerDimensions>
                                </div>
                            </Grid>
                        </Grid>
                        <div style={{ color: 'white', marginTop: 10}}>
                            Source of stocks <a href="https://www.quandl.com/data/EURONEXT-Euronext-Stock-Exchange">here</a>
                        </div>
                    </Grid>
                </Grid>
            </div>


            <Dialog
                modal={false}
                open={this.props.settings.isAddDialogOpen}
                onClose={this.handleAddDialogCancelClose.bind(this)}
                >
                <DialogTitle>Add a new Stock Exchange</DialogTitle>
                <DialogContent>
                <FormControl
                    style={styles.formControl}>
                    <InputLabel htmlFor="stockExchange">Stock Exchange</InputLabel>
                    <TextField
                        hintText="Stock Exchange"
                        onChange={this.handleTextFieldChange.bind(this)}/>
                </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button
                        color="secondary"
                        primary={true}
                        onClick={this.handleAddDialogCancelClose.bind(this)}>
                      Cancel
                      </Button>
                      <Button
                        color="primary"
                        primary={true}
                        onClick={this.handleAddDialogClose.bind(this)}>
                        Add it !
                      </Button>
                </DialogActions>
            </Dialog>

            <Dialog
              open={this.props.settings.isRemoveDialogOpen}
              onClose={this.handleDialogCancelClose.bind(this)}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description">
              <DialogTitle id="alert-dialog-title">Are you sure?</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  If you continue, the Stock Exchange will be permanently deleted.
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button
                    color="secondary"
                    primary={true}
                    onClick={this.handleDialogCancelClose.bind(this)}>
                    Cancel
                  </Button>
                  <Button
                    color="primary"
                    primary={true}
                    onClick={this.handleDialogClose.bind(this)}>
                    Yes, delete it !
                  </Button>
              </DialogActions>
            </Dialog>

            <Dialog
                modal={false}
                open={this.props.settings.isAddStockDialogOpen}
                onClose={this.handleAddStockDialogCancelClose.bind(this)}
                >
                <DialogTitle>Add a new Stock</DialogTitle>
                <DialogContent>
                <FormControl
                    style={styles.formControl}>
                    <InputLabel htmlFor="stock">Stock</InputLabel>
                    <TextField
                        hintText="Stock"
                        onChange={this.handleStockFieldChange.bind(this)}/>
                </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button
                        color="secondary"
                        primary={true}
                        onClick={this.handleAddStockDialogCancelClose.bind(this)}>
                        Cancel
                      </Button>
                      <Button
                        color="primary"
                        primary={true}
                        onClick={this.handleAddStockDialogClose.bind(this)}>
                        Add it !
                      </Button>
                </DialogActions>
            </Dialog>

            <Snackbar
              open={this.props.settings.isNoExchangeSelectionSnackbarOpen}
              message="You must select a Stock Exchange"
              autoHideDuration={4000}
              onClose={this.handleNoExchangeSelectionSnackbarClose.bind(this)}
            />

            <Snackbar
              open={this.props.settings.isSnackbarOpen}
              message="The name must be longer than 2"
              autoHideDuration={4000}
              onClose={this.handleSnackbarClose.bind(this)}
            />

            <Snackbar
              open={this.props.settings.isNoSelectionSnackbarOpen}
              message="You must select at least 1 Stock to delete"
              autoHideDuration={4000}
              onClose={this.handleNoSelectionSnackbarClose.bind(this)}
            />
        </div>
		)
	}
}
