import React from "react";
import { connect } from "react-redux";
import ReactDOM from 'react-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import AppMenu from './AppMenu.js';
import AppPageHome from './AppPageHome.js';
import AppPageConsult from './AppPageConsult.js';
import AppPageDashboard from './AppPageDashboard.js';
import AppPageAssessment from './AppPageAssessment.js';
import AppPageBacktest from './AppPageBacktest.js';
import AppPageSettings from './AppPageSettings.js';
import AppPageTimetable from './AppPageTimetable.js';
import AppToolbar from './AppToolbar.js';
import axios from 'axios';
import { instanceOf, PropTypes } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';

@connect((store) => {
    return {
        global : store.global
    };
})
class App extends React.Component {

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired,
        persistor: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.updateDimensions = this.updateDimensions.bind(this);
        //const { persistor } = this.props;
        //persistor.pause();

        //this.props.dispatch({type: "SET_COOKIE_HANDLER", payload: cookies});
        //if (cookies.get('state') != null)
        //    this.props.dispatch({type: "SET_APP_STATE", payload: cookies.get('state')});
    }

    updateDimensions() {
        this.props.dispatch({type:"SET_SIZE",
                            payload:{
                                width: window.innerWidth-50,
                                height: window.innerHeight-100
                            }});
    }
    componentWillMount() {
        this.updateDimensions();
    }
    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
        axios.get('getStockExchanges').then(response => {
            this.props.dispatch({type: "GL_SET_STOCKEXCHANGES", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    render() {
        const { cookies, persistor } = this.props;

        var theme = createMuiTheme({
          palette: {
            type: "dark",
            primary: {main: "#00bcd4"},
            secondary: {main: "#ff4081"}
          }
        });

        var page = null;
        if (this.props.global != null) {
            if (this.props.global.activePage == 'consultation') {
              page = <AppPageConsult />;
            } else if (this.props.global.activePage == 'dashboard') {
              page = <AppPageDashboard />;
            } else if (this.props.global.activePage == 'assessment') {
              page = <AppPageAssessment />;
            } else if (this.props.global.activePage == 'backtest') {
              page = <AppPageBacktest />;
            } else if (this.props.global.activePage == 'settings') {
              page = <AppPageSettings />;
              } else if (this.props.global.activePage == 'timetable') {
              page = <AppPageTimetable />;
            } else {
              page = <AppPageHome />;
            }
        } else {
            page = <AppPageHome />;
        }

        return(
            <div id="body">
                <div id="menu">
                    <MuiThemeProvider theme={theme}>
                        <AppMenu />
                    </MuiThemeProvider>
                </div>
                <div id="toolbar">
                    <MuiThemeProvider theme={theme}>
                        <AppToolbar
                            persistor={persistor}/>
                    </MuiThemeProvider>
                </div>
                <div id="page-content"
                    width={this.props.global.size.width}
                    height={this.props.global.size.height}>
                    <MuiThemeProvider theme={theme}>
                        {page}
                    </MuiThemeProvider>
                </div>
            </div>
        );
    }

}

export default withCookies(App);