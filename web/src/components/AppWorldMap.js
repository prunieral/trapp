import React, { Component } from "react"
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
  Markers,
  Marker,
} from "react-simple-maps"
import { Motion, spring } from "react-motion";
import PropTypes from "prop-types";
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import ReactSvgTimer from 'react-svg-timer';
import AppTimerMarker from './AppTimerMarker.js';
import Button from '@material-ui/core/Button';


class AppWorldMap extends Component {
  constructor() {
    super()
    this.state = {
      center: [0,20],
      zoom: 1,
    }
    this.handleZoomIn = this.handleZoomIn.bind(this)
    this.handleZoomOut = this.handleZoomOut.bind(this)
    this.handleCityClick = this.handleCityClick.bind(this)
    this.handleReset = this.handleReset.bind(this)
  }
  handleZoomIn() {
    this.setState({
      zoom: this.state.zoom * 2,
    })
  }
  handleZoomOut() {
    this.setState({
      zoom: this.state.zoom / 2,
    })
  }
  handleCityClick(city) {
    this.setState({
      zoom: 2,
      center: city.coordinates,
    })
  }
  handleReset() {
    this.setState({
      center: [0,20],
      zoom: 1,
    })
  }
  render() {
    var { height, width, data } = this.props;
    console.log(data);

    return (
    <div style={{
                width: width,
                height: height,
              }}>
        <Button
            variant="contained"
            color="primary"
            onClick={this.handleReset}
            style={{ color: 'white', top: 20, right: 30, position: 'absolute' }}>
            RESET
        </Button>
        <Motion
          defaultStyle={{
            zoom: 1,
            x: 0,
            y: 20,
          }}
          style={{
            zoom: spring(this.state.zoom, {stiffness: 210, damping: 20}),
            x: spring(this.state.center[0], {stiffness: 210, damping: 20}),
            y: spring(this.state.center[1], {stiffness: 210, damping: 20}),
          }}
          >
          {({zoom,x,y}) => (
            <ComposableMap
              projectionConfig={{ scale: 205 }}
              style={{
                width: '100%',
                height: '100%',
              }}
              >
              <ZoomableGroup center={[x,y]} zoom={zoom}>
                <Geographies geography="./json/map.json">
                  {(geographies, projection) =>
                    geographies.map((geography, i) => geography.id !== "010" && (
                      <Geography
                        key={i}
                        geography={geography}
                        projection={projection}
                        style={{
                          default: {
                            fill: "#ECEFF1",
                            stroke: "#607D8B",
                            strokeWidth: 0.75,
                            outline: "none",
                          },
                          hover: {
                            fill: "#CFD8DC",
                            stroke: "#607D8B",
                            strokeWidth: 0.75,
                            outline: "none",
                          },
                          pressed: {
                            fill: "#ff4081",
                            stroke: "#607D8B",
                            strokeWidth: 0.75,
                            outline: "none",
                          },
                        }}
                      />
                  ))}
                </Geographies>
                <Markers>
                  {data.map((marker, i) => (
                    <AppTimerMarker
                      key={i}
                      marker={marker}
                      name={marker['name']}
                      onClick={this.handleCityClick}
                      timerCount={marker['time_left']}
                      >
                    </AppTimerMarker>
                  ))}
                </Markers>
              </ZoomableGroup>
            </ComposableMap>
          )}
        </Motion>
      </div>
    )
  }
}

AppWorldMap.propTypes = {
	height: PropTypes.number.isRequired,
	width: PropTypes.number.isRequired,
	data: PropTypes.array,
};

AppWorldMap.defaultProps = {
    data: []
};

export default AppWorldMap