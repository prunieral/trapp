import React, {Component}  from 'react';
import Icon from '@material-ui/core/Icon';
import { connect } from 'react-redux';
import SaveIcon from '@material-ui/icons/Save';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { Cookies } from 'react-cookie';
import { PropTypes } from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';

@connect((store) => {
    return {
        global : store.global,
    }
})
export default class AppToolbar extends Component {

    static propTypes = {
        persistor: PropTypes.object.isRequired
    };

    save() {
        var { persistor } = this.props;
        if (this.props.global.saveMode == false) {
            this.props.dispatch({type: "SET_SAVE_STATE", payload: true});
            this.props.dispatch({type: "SET_SAVE_SNACKBAR_STATE", payload: true});
            persistor.persist()
        } else {
            this.props.dispatch({type: "SET_SAVE_STATE", payload: false});
            this.props.dispatch({type: "SET_SAVE_SNACKBAR_STATE", payload: true});
            persistor.pause()
        }
    }

    remove() {
        var { persistor } = this.props;
        persistor.purge();
        location.reload();
    }

    handleSnackbarClose() {
        this.props.dispatch({type: "SET_SAVE_SNACKBAR_STATE", payload: false});
    }

    render() {

        var styles = {
            activatedButton: {
                color: "#00bcd4",
            }
        }

        return (
        <div>
            <a id="toolbar-save"
                className={'toolbar-icon'}
                onClick={this.save.bind(this)}
                style={this.props.global.saveMode ? styles.activatedButton : null}>
                <SaveIcon/>
            </a>
            <a id="toolbar-save"
                className={'toolbar-icon'}
                onClick={this.remove.bind(this)}>
                <DeleteForeverIcon/>
            </a>

            <Snackbar
              open={this.props.global.isSaveSnackbarOpen}
              message="The workspace was saved successfully"
              autoHideDuration={4000}
              onClose={this.handleSnackbarClose.bind(this)}
            />
        </div>);
    }
}