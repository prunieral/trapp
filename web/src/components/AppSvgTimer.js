import * as React from "react";
import { interval } from "rxjs";
import moment from "moment";
import TimerSVG from "react-svg-timer";

export type Props = {
  completeTimer?: (param: boolean) => void;
  resetTimer?: () => void;
  color: string;
  timerCount: number;
  timerDuration?: (param: number) => void;
  name: string;
};

export type State = {
  timerIsRunning: boolean;
};

export default class AppSvgTimer extends React.Component<Props, State> {
   constructor(props: Props) {
    super(props);

    // changes to state variables will cause the browser to re-render
    this.state = {
      timerIsRunning: false
    };

    // bind event handlers
    this.toggleStart = this.toggleStart.bind(this);
  }

  // other variables are fine as instance variables
  initialCounter: number;
  // constants for calculations of the SVG circle
  goalTimeMillis: number;
  degrees: number;
  /*
  This property stores the Moment.js moment that the timer was started.
  Rather than rely on the observable timout for keeping time (where 1ms
  might not actually be 1ms due to system load etc.), the time is tracked
  with moments.
  */
  startDateMoment: any;
  // the milliseconds since the timer was started (without considering pauses)
  timerDuration: number;
  // the "official" milliseconds - excluding time during pauses
  elapsedTime: number;
  // start in a 'reset' stat
  timerisReset: boolean;
  // this property will store the Observable for the timer countdown
  timerObservable: any;
  timerResetObservable: any;

  componentDidMount(): void {
    // set instance variables
    var { timerCount } = this.props;
    this.initialCounter = timerCount;
    console.log(this.initialCounter)
    this.goalTimeMillis = this.initialCounter * 1000;
    this.startDateMoment = null;
    this.timerDuration = 0;
    this.elapsedTime = 0;
    this.timerisReset = true;

    this.timerObservable = null;

    /*
		This observable listens for a change in the resetTimerRequested prop.
		It calls the local reset() function to reset local state, and also
		the parent component's callback function to reset parental state.
	*/
    this.timerResetObservable = interval(10).subscribe(_t => {
      if (this.props.resetTimerRequested) {
        this.reset();
        // call callback function in parent component
        if (this.props.resetTimer) {
          this.props.resetTimer();
        }
      }
    });

    this.reset();
    this.start();
  }

  componentWillUnmount(): void {
    this.reset();
  }

  reset(): void {
    this.timerisReset = true;
    this.timerDuration = 0;
    this.elapsedTime = 0;
    // re-render required
    this.setState({
      timerIsRunning: false
    });
    // we don't want multiple instances of the timer hanging around
    if (this.timerObservable) {
      this.timerObservable.unsubscribe();
    }
    // call the callback function in the parent component to reset state
    if (this.props.completeTimer) {
      this.props.completeTimer(false);
    }
  }

  toggleStart(): void {
    if (this.state.timerIsRunning) {
      this.pause();
    } else {
      this.start();
    }
  }

  start(): void {
    /*
	This could be called from reset state, or simply unpausing.
	*/

    // get the current moment to calculate the elapsed time
    let currentDate: Date = new Date();
    this.startDateMoment = currentDate;

    // if un-pausing, need to reset the elapsedTime property
    // it is no longer zero, but whatever the timer currently shows
    if (!this.timerisReset) {
      this.elapsedTime = this.timerDuration;
    }

    // prepare for any further unpauses
    this.timerisReset = false;

    // and we're off (again - if this is an un-pause)
    this.setState({
      timerIsRunning: true
    });

    this.timerObservable = interval(1000).subscribe(_t => {

      this.initialCounter -= 1;

      if (this.initialCounter < 0) {
        if (this.props.completeTimer) {
          this.props.completeTimer(true);
        }
        this.update();
      }

      if (this.props.timerDuration) {
        this.props.timerDuration(this.timerDuration);
      }

      this.update();
    });
  }

  pause(): void {
    this.setState({
      timerIsRunning: false
    });

    if (this.timerObservable) {
      this.timerObservable.unsubscribe();
    }
  }

  update(deg: number): void {
        this.setState({});
  }

  timerText(): string {
        return Math.floor(this.initialCounter / 3600) +
            ":" + Math.floor(this.initialCounter / 60) % 60 +
            ":" + this.initialCounter % 60;
  }

  render(): JSX.Element {
    // Destructuring props provides an opportunity to set defaults
    const {
      name,
      color
    } = this.props;
    // the SVG is deterministic, so is split out into a stateless component
    return (
        <text
          textAnchor="middle"
          style={{ fill: color, fontSize: "12px", fontWeight: "bold" }}
        >
           {name} : {this.timerText()}
        </text>
    );
  }
}