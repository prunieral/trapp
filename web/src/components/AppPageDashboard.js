import React, {Component}  from 'react';
import ReactDOM from 'react-dom';
import ReactGridLayout from 'react-grid-layout';
import AppCandleStickChart from './AppCandleStickChart.js';
import axios from 'axios';
import { contains, getData } from "../utils.js";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import ToolbarGroup from '@material-ui/core/Toolbar';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import Checkbox from '@material-ui/core/Checkbox';
import { connect } from "react-redux";
import ContainerDimensions from 'react-container-dimensions';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import MenuList from '@material-ui/core/MenuList';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';
import SettingsIcon from '@material-ui/icons/Settings';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';

var styles = {
          menubar: {
                height: 35,
                padding: "0 15px",
          },
          checkbox: {
                maxWidth: 40,
          },
          smallIcon: {
                width: 30,
                height: 30,
                color: "white",
                padding: "0",
          },
          smallIcon2: {
                width: 30,
                height: 30,
                color: "white",
                padding: "0",
          },
          toolbar: {
                background: "black",
                height: 35,
                minHeight: 35,
                padding: "0px 5px"
          },
          toolbarButtons: {
                transform: "scale(0.8)",
                padding: "0px 0px"
          },
          toolButtons: {
                transform: "scale(0.7)"
          },
          formControl: {
                minWidth: 200,
          },
          grow: {
                flexGrow: 1,
          },
    };

@connect((store) => {
    return {
        dashboard : store.pagedashboard,
        global : store.global
    }
})
export default class AppPageDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = { 'menuEl' : {} }
        console.log('FF');
        console.log(this.props.dashboard.layout);
    }

    addColor() {
        styles.smallIcon.color = '#ff4081';
    }

    removeColor() {
        styles.smallIcon.color = 'white';
    }

    addColor2() {
        styles.smallIcon2.color = '#ff4081';
    }

    removeColor2() {
        styles.smallIcon2.color = 'white';
    }

    handleChange(id, event, index, value) {
        this.props.dispatch({type: "DASHBOARD_SET_INTERVAL", payload:
            {id: id,
            value: value}});
        this.setState({});
	}

	addElement(e) {
	    this.props.dispatch({type: "DASHBOARD_ADD_ELEMENT"});
	    this.setState({});
	}

	removeElement(id, e) {
	    this.props.dispatch({type: "DASHBOARD_REMOVE_ELEMENT", payload: id});
	    this.setState({});
	}

	removeAllElements(e) {
	    this.props.dispatch({type: "DASHBOARD_REMOVE_ALL_ELEMENTS"});
	    this.setState({});
	}

	configurateElement(id, e) {
	    this.props.dispatch({type: "DASHBOARD_CONFIGURATE_ELEMENT", payload: id});
	    this.setState({});
	}

	saveConfiguration(e) {
	    this.props.dispatch({type: "DASHBOARD_SAVE_CONFIGURATION"});
	    this.setState({});
	}

	onResizeStop(e) {
	    this.props.dispatch({type: "DASHBOARD_SAVE_STATE", payload: e});
        this.setState({});
    }

    onDragStop(e) {
	    this.props.dispatch({type: "DASHBOARD_SAVE_STATE", payload: e});
        this.setState({});
    }

    cancelDialog() {
        this.props.dispatch({type: "DASHBOARD_CANCEL_DIALOG"});
    }

    validateDialog() {
        this.props.dispatch({type: "DASHBOARD_SET_INFO_OF_DATA",
            payload: {stockExchange: this.props.dashboard.stockExchange,
                      security: this.props.dashboard.security}});
        getData(this.props.dashboard.stockExchange, this.props.dashboard.security).then(data => {
			this.props.dispatch({type: "DASHBOARD_SET_DATA", payload: data});
			this.setState({});
		})
        this.props.dispatch({type: "DASHBOARD_VALIDATE_DIALOG"});
    }

    handleUpdateStockExchange(event) {
        this.props.dispatch({type: "DASHBOARD_CHANGE_STOCKEXCHANGE", payload: event.target.value});
        if (contains(this.props.dashboard.stockExchanges, event.target.value) > -1) {
            axios.get("getSecurities?stockexchange=" + event.target.value).then(response => {
                this.props.dispatch({type: "DASHBOARD_SET_SECURITIES", payload: response.data});
            }).catch((error) => {
                console.log(error)
            });
        }
    }

    handleUpdateSecurity(event) {
        this.props.dispatch({type: "DASHBOARD_CHANGE_SECURITY", payload: event.target.value});
    }

    handleOpenMenu(event, key) {
        var newData = this.state.menuEl;
        newData[key] = event.currentTarget;
        this.setState({ menuEl: newData });
    }

    handleCloseMenu(key) {
        var newData = this.state.menuEl;
        newData[key] = null;
        this.setState({ menuEl: newData });
    }

    componentDidMount() {
        axios.get('getStockExchanges').then(response => {
            this.props.dispatch({type: "DASHBOARD_SET_STOCKEXCHANGES", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
	}

	handlePress(e) {
        e.stopPropagation();
    }

    render() {
		return (
		<div className="page">
		    <div className="title-page">Dashboard</div>
		    <div
		        id="pageDashboard-Tools"
		        style={styles.toolButtons}>
                <List>
                    <ListItem>
                      <Button
                        variant="fab"
                        color="primary"
                        aria-label="Add"
                        onClick={this.addElement.bind(this)}>
                        <AddIcon />
                      </Button>
                    </ListItem>
                    <ListItem>
                      <Button
                        variant="fab"
                        color="secondary"
                        aria-label="Delete"
                        onClick={this.removeAllElements.bind(this)}>
                        <DeleteIcon />
                      </Button>
                    </ListItem>
                  </List>
            </div>
            <ReactGridLayout {...this.props}
                className="layout"
                layout={this.props.dashboard.layout}
                onResizeStop={this.onResizeStop.bind(this)}
                onDragStop={this.onDragStop.bind(this)}
                cols={16}
                rowHeight={80}
                width={this.props.global.size.width}>
                {this.props.dashboard.layout.map((item, index) => (
                    <div key={item.i} className={"chart-" + item.i} data-grid={{draggableHandle: '.draggable'}}>
                        <div className="draggable">
                        <AppBar
                            position="static"
                            color="default"
                            style={styles.toolbar}>
                            <Toolbar
                                style={styles.toolbar}
                                variant="dense">
                                <Typography>
                                    {item.stockExchange}/{item.security}
                                </Typography>
                                <div style={styles.grow} />
                                <ToolbarGroup
                                    style={styles.toolbarButtons}>
                                    <IconButton
                                    style={styles.toolbar}
                                    aria-label="Configure"
                                    onClick={this.configurateElement.bind(this, item.i)}>
                                        <SettingsIcon />
                                      </IconButton>
                                    <IconButton
                                        style={styles.toolbar}
                                        aria-label="Delete"
                                        onClick={this.removeElement.bind(this, item.i)}>
                                        <DeleteIcon />
                                      </IconButton>
                                  </ToolbarGroup>
                              </Toolbar>
                            </AppBar>
                        </div>
                        <div className={"pageDashboard-ElBody"} ref="chartArea"
                            onMouseDown={e => this.handlePress(e)}>
                            <ContainerDimensions>
                                { ({ width, height }) =>
                                <AppCandleStickChart
                                    data={item.data}
                                    interval={item.interval}
                                    width={width}
                                    height={height}/>
                                }
                            </ContainerDimensions>
                        </div>
                    </div>
                ))}
            </ReactGridLayout>
            <Dialog
                modal={false}
                open={this.props.dashboard.openDialog}
                onRequestClose={this.saveConfiguration.bind(this)}
                >
                <DialogTitle>Choose the Exchange and its Security</DialogTitle>
                <DialogContent>
                <FormControl
                    style={styles.formControl}>
                    <InputLabel htmlFor="stockExchange">Stock Exchange</InputLabel>
                    <Select
                      value={this.props.dashboard.stockExchange}
                      onChange={this.handleUpdateStockExchange.bind(this)}>
                          {this.props.dashboard.stockExchanges.map(function(exchange){
                            return <MenuItem value={exchange}>{exchange}</MenuItem>;
                          })}
                    </Select>
                </FormControl>
                <FormControl
                    style={styles.formControl}>
                    <InputLabel htmlFor="security">Security</InputLabel>
                    <Select
                      value={this.props.dashboard.security}
                      onChange={this.handleUpdateSecurity.bind(this)}>
                          {this.props.dashboard.securities.map(function(security){
                            return <MenuItem value={security}>{security}</MenuItem>;
                          })}
                    </Select>
                </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button
                        color="secondary"
                        onClick={this.cancelDialog.bind(this)}>
                      Cancel
                      </Button>
                      <Button
                         color="primary"
                        disabled={this.props.dashboard.security == '' || this.props.dashboard.stockExchange == ''}
                        onClick={this.validateDialog.bind(this)}>
                        OK
                       </Button>
                  </DialogActions>
            </Dialog>
        </div>
		)
	}
}
