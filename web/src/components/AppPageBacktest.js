import React, {Component}  from 'react';
import ReactDOM from 'react-dom';
import { contains, getData } from "../utils.js";
import { TypeChooser } from "react-stockcharts/lib/helper";
import { connect } from "react-redux";
import axios from 'axios';
import ContainerDimensions from 'react-container-dimensions';
import AceEditor from 'react-ace';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { DatePicker } from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider } from "material-ui-pickers";
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import Button from '@material-ui/core/Button';

@connect((store) => {
    return {
        backtest : store.pagebacktest,
        global : store.global
    }
})
export default class AppPageBacktest extends Component {

    constructor(props) {
        super(props);
        this.state = { capital : this.props.backtest.capital,
                        startDate: this.props.backtest.startDate,
                        minComission: this.props.backtest.minComission,
                        percentComission: this.props.backtest.percentComission
                     }
    }

    componentDidMount() {
	}

	handleChange = name => event => {
        this.setState({ [name]: event.target.value, });
        this.props.dispatch({ type: "BACKTEST_SET_FIELD_VALUE", payload: {[name]: event.target.value} });
    };

    render() {

        var styles = {
            textLabel : {
                color: 'white',
                fontSize: 13,
            },
        }

		return (
		<div className="page">
		    <div className="title-page">Backtest</div>

		    <Grid container spacing={16} justify={'center'}>
                <Grid item xs={12} sm={6} md={6} lg={4}>
                    <div className="subtitle-page">Model selection</div>
                    {Object.keys(this.props.backtest.selectedModel).length > 0 && (
                        <Grid container spacing={16} style={styles.textLabel}>
                            <Grid item xs={6} sm={6}>Assessment :</Grid>
                            <Grid item xs={6} sm={6}>{this.props.backtest.selectedModel.assessment}</Grid>
                            <Grid item xs={6} sm={6}>Model ID :</Grid>
                            <Grid item xs={6} sm={6}>{this.props.backtest.selectedModel.id}</Grid>
                            <Grid item xs={6} sm={6}>Algorithm :</Grid>
                            <Grid item xs={6} sm={6}>{this.props.backtest.selectedModel.algorithm}</Grid>
                            <Grid item xs={6} sm={6}>Parameters :</Grid>
                            <Grid item xs={6} sm={6}>
                                {Object.entries(this.props.backtest.selectedModel.params).map(function([key, value]) {
                                    return (<div>{key.split('__')[1]} = {value}</div>);
                                })}
                            </Grid>
                        </Grid>
                    )}
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={4}>
                    <div className="subtitle-page">Portfolio properties</div>
                    <Grid container
                        spacing={8}
                        style={styles.textLabel}
                        alignItems="center"
                        justify="center">
                        <Grid item xs={12} sm={6}>
                            <TextField fullWidth
                              label="Capital"
                              value={this.state.capital}
                              onChange={this.handleChange('capital')}
                              type="number"
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DatePicker
                                    keyboard
                                    clearable
                                    label="Start date"
                                    format="dd/MM/yyyy"
                                    maxDate={new Date()}
                                    value={this.state.startDate}
                                    onChange={this.handleChange('startDate')}/>
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField fullWidth
                              label="Comission in %"
                              value={this.state.percentComission}
                              onChange={this.handleChange('percentComission')}
                              type="number"
                              InputLabelProps={{
                                  shrink: true,
                              }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField fullWidth
                              label="Minimum of Comission"
                              value={this.state.minComission}
                              onChange={this.handleChange('minComission')}
                              type="number"
                              InputLabelProps={{
                                  shrink: true,
                              }}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item  xs={12} sm={12} md={12} lg={8}>
                    <Button
                        variant="contained"
                        color="primary"
                        style={{ color: 'white', float: 'right', marginTop: 10, marginRight: 10 }}>
                        Go !
                    </Button>
                </Grid>


                <Grid item xs={12} sm={12}>
                    <div className="subtitle-page">Results</div>
                    ALOOOO
                </Grid>
            </Grid>
        </div>
		)
	}
}