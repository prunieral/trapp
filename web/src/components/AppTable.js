import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';

let counter = 0;

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

class EnhancedTableHead extends React.Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const { onSelectAllClick, order, orderBy, numSelected, rowCount, columns, selection, action1Button } = this.props;

    return (
      <TableHead>
        <TableRow>
          { selection && (
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          )}
          { action1Button && (
          <TableCell padding="actions">
          </TableCell>
          )}
          {columns.map(column => {
            return (
              <TableCell
                key={column}
                numeric={true}
                padding={'default'}
                sortDirection={orderBy === column ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={'bottom-end'}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === column}
                    direction={order}
                    onClick={this.createSortHandler(column)}
                  >
                    {column}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            );
          }, this)}
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  columns: PropTypes.array.isRequired,
  selection: PropTypes.bool.isRequired,
  action1Button: PropTypes.bool.isRequired,
};

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
    width: 120,
  },
  title: {
    flex: '0 0 auto',
    width: 'calc(100% - 120px)'
  },
});

let EnhancedTableToolbar = props => {
  const { numSelected, classes, name, onAdd, onDelete } = props;

  return (
    <Toolbar
      className={classNames(classes.root)}
    >
      <div className={classes.title}>
          <Typography variant="h6" id="tableTitle">
            {name}
          </Typography>
      </div>
      <div className={classes.actions}>
        {(typeof onAdd !== "undefined" && onAdd != null) ? (
          <Button
            variant="fab"
            color="primary"
            aria-label="Add"
            onClick={onAdd}
            style={{ transform : 'scale(0.6)'}}>
            <AddIcon />
          </Button>
        ): ""}
        {(typeof onDelete !== "undefined" && onDelete != null) ? (
          <Button
            variant="fab"
            color="secondary"
            aria-label="Delete"
            onClick={onDelete}
            style={{ transform : 'scale(0.6)'}}>
            <DeleteIcon />
          </Button>
        ): ""}
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  onAdd: PropTypes.function,
  onDelete: PropTypes.function,
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {

  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

class EnhancedTable extends React.Component {
  state = {
    order: 'asc',
    orderBy: 'calories',
    selected: [],
    page: 0,
    rowsPerPage: 5,
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.data.map(n => n.id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleAction1Click = (event, id, onClick) => {
      if (onClick != 'undefined' && onClick != null) {
            onClick(id);
      }
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    var { classes, name, data, width, onAdd,
          onDelete, selection, action1Button, onAction1Click, iconAction1 } = this.props;
    var { order, orderBy, selected, rowsPerPage, page } = this.state;
    var emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);
    var noMargin = { margin: 0 }
    var columns = data.length > 0 ? Object.keys(data[0]) : [];

    onAdd = typeof onAdd === "undefined" || onAdd == null ? null : onAdd.bind(this);
    onDelete = typeof onDelete === "undefined" || onDelete == null ? null : onDelete.bind(this, this.state.selected);

    return (
      <Paper
        className={classes.root}
        style={noMargin}>
        <EnhancedTableToolbar
            numSelected={selected.length}
            name={name}
            onAdd={onAdd}
            onDelete={onDelete}/>
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
              columns={columns}
              selection={selection}
              action1Button={action1Button}
            />
            <TableBody>
              {stableSort(data, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(n => {
                  const isSelected = this.isSelected(n.id);
                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, n.id)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.id}
                      selected={isSelected}
                    >
                      {selection &&
                      <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                      </TableCell>
                      }
                      {action1Button &&
                        <TableCell>
                            <a id="client"
                            className={'menu-item'}
                            onClick={event => this.handleAction1Click(event, n.id, onAction1Click)}>
                                {iconAction1}
                            </a>
                        </TableCell>
                        }
                    {Object.keys(n).map(key => {
                        return (
                            <TableCell>
                            {(() => {
                            if (key == 'id' && typeof(n[key]) == 'number') {
                                return(String(n[key].toFixed()))
                            } else if ((typeof(n[key]) == 'number')) {
                                return(String(n[key].toFixed(3)))
                            } else {
                                return(String(n[key]))
                            }
                            })()}
                            </TableCell>)
                    })}
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  width: PropTypes.number.isRequired,
  onAdd: PropTypes.function,
  onDelete: PropTypes.function,
  action1Button: PropTypes.boolean,
  onAction1Click: PropTypes.function,
  iconAction1: PropTypes.object,
  selection: PropTypes.bool,
};

EnhancedTable.defaultProps = {
	onAdd: null,
	onDelete: null,
	action1Button: false,
	onAction1Click: null,
	iconAction1: '<div/>',
	selection: false,
};

export default withStyles(styles)(EnhancedTable);