import React, {Component}  from 'react';
import ReactDOM from 'react-dom';

export default class AppLoading extends Component {

    render() {
		return (
		<div className="loader">
            <div className="spinner">
                <div className="double-bounce1"></div>
                <div className="double-bounce2"></div>
            </div>
        </div>
		)
	}
}
