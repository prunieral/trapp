import React, {Component}  from 'react';
import ReactDOM from 'react-dom';
import AppCandleStickChart from './AppCandleStickChart.js';
import { getData, contains } from "../utils.js";
import { TypeChooser } from "react-stockcharts/lib/helper";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { connect } from "react-redux";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

@connect((store) => {
    return {
        store : store.pagehome
    }
})
export default class AppPageHome extends Component {

    constructor(props) {
        super(props);
        this.state = {
          chartData: [],
          stockExchanges: [],
          securities: [],
          stockExchange: "NASDAQ",
          security: "AAAP",
          chartAreaWidth : 100,
          chartAreaHeight : 100
        };

        this.handleUpdateStockExchange = this.handleUpdateStockExchange.bind(this);
        this.handleUpdateSecurity = this.handleUpdateSecurity.bind(this);
    }

    handleUpdateStockExchange(value) {
        this.state.stockExchange = value;
        fetch("getSecurities?stockexchange=" + value)
      .then(function(response) {
        if (response.status >= 400) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then(function(data) {
        this.setState({ securities: data });
      }.bind(this));
    }

    handleUpdateSecurity(value) {
        this.state.security = value;
        if (contains(this.state.securities, value) >= 0) {
            getData(this.state.stockExchange, value).then(data => {
			    this.setState({ chartData : data });
		    })
		}
    }

    componentDidMount() {
	  fetch("getStockExchanges")
      .then(function(response) {
        if (response.status >= 400) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then(function(data) {
        this.setState({ stockExchanges: data });
      }.bind(this));
	}

	setDashboardPage() {
	    this.props.dispatch({type: "SET_DASHBOARD_PAGE", payload: null});
	}

	setAssessmentPage() {
	    this.props.dispatch({type: "SET_ASSESSMENT_PAGE", payload: null});
	}

	setBacktestPage() {
	    this.props.dispatch({type: "SET_BACKTEST_PAGE", payload: null});
	}

    render() {

        var styles = {
            spanText : {
                color: 'white',
                paddingBottom: 20,
                lineHeight: 1.3
            },
            spanCard : {
                'display': '-webkit-box',
                '-webkit-line-clamp': '3',
                '-webkit-box-orient': 'vertical',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                height: 63,
            },
        };

		return (
		<div className="page">
		    <div className="title-page">Home</div>
		    <div id="pageConsult-Header">
		        <Grid container
		            alignItems="center"
		            justify="center">
		            <Grid item
		                xs={12} sm={8} md={6} lg={6}>
		                <div className="subtitle-page">About Trapp</div>
		                <div style={styles.spanText}>
		                <span style={styles.spanText}>Trapp is an app made to visualize the results of Machine Learning applied on daily stock data. The aim is to find the most effective model via metrics and backtests and to simulate a portfolio with the past data.</span>
		                </div>
		                <Grid container
		                    spacing={8}
		                    style={{ marginBottom : 30 }}>
		                    <Grid item
		                        xs={12} sm={4} md={4} lg={4}>
		                        <Card>
                                  <CardActionArea>
                                    <CardMedia
                                      component="img"
                                      alt="Dashboard"
                                      height="100"
                                      image="./image/dashboard.jpg"
                                      title="Dashboard"
                                    />
                                    <CardContent>
                                      <Typography gutterBottom variant="h6" component="h2"
                                        style={{ fontVariant: 'small-caps' }}>
                                        Dashboard
                                      </Typography>
                                      <Typography component="p" style={styles.spanCard}>
                                        Consult the daily updated stock charts via an advanced dashboard
                                      </Typography>
                                    </CardContent>
                                  </CardActionArea>
                                  <CardActions>
                                    <Button size="small" color="primary" onClick={this.setDashboardPage.bind(this)}>
                                      Go to page
                                    </Button>
                                  </CardActions>
                                </Card>
		                    </Grid>
		                    <Grid item
		                        xs={12} sm={4} md={4} lg={4}>
		                        <Card>
                                  <CardActionArea>
                                    <CardMedia
                                      component="img"
                                      alt="Model assessment"
                                      height="100"
                                      image="./image/code.jpeg"
                                      title="Model assessment"
                                    />
                                    <CardContent>
                                      <Typography gutterBottom variant="h6" component="h2"
                                        style={{ fontVariant: 'small-caps' }}>
                                        Model assessment
                                      </Typography>
                                      <Typography component="p" style={styles.spanCard}>
                                        Analyse the machine learning algorithm effetiveness applied on stock data
                                      </Typography>
                                    </CardContent>
                                  </CardActionArea>
                                  <CardActions>
                                    <Button size="small" color="primary" onClick={this.setAssessmentPage.bind(this)}>
                                      Go to page
                                    </Button>
                                  </CardActions>
                                </Card>
		                    </Grid>
		                    <Grid item
		                        xs={12} sm={4} md={4} lg={4}>
		                        <Card>
                                  <CardActionArea>
                                    <CardMedia
                                      component="img"
                                      alt="Backtest"
                                      height="100"
                                      image="./image/chart.jpg"
                                      title="Backtest"
                                    />
                                    <CardContent>
                                      <Typography gutterBottom variant="h6" component="h2"
                                        style={{ fontVariant: 'small-caps' }}>
                                        Model backtest
                                      </Typography>
                                      <Typography component="p" style={styles.spanCard}>
                                        Simulate a portfolio with the past data and the machine learning predictions
                                      </Typography>
                                    </CardContent>
                                  </CardActionArea>
                                  <CardActions>
                                    <Button size="small" color="primary" onClick={this.setBacktestPage.bind(this)}>
                                      Go to page
                                    </Button>
                                  </CardActions>
                                </Card>
		                    </Grid>
		                </Grid>

                        <div className="subtitle-page">Model assessment features</div>
		                <Grid container
		                    spacing={8}
		                    style={{ marginBottom : 30 }}>
		                    <Grid item
		                        xs={12} sm={4} md={4} lg={4}>
		                        <Card>
                                  <CardActionArea>
                                    <CardContent>
                                      <Typography gutterBottom variant="h6" component="h2"
                                        style={{ fontVariant: 'small-caps' }}>
                                        +10 indicators
                                      </Typography>
                                      <Typography component="p" style={styles.spanCard}>
                                        Includes normalized financial indicators such as CCI or Chaikin oscillator
                                      </Typography>
                                    </CardContent>
                                  </CardActionArea>
                                </Card>
		                    </Grid>
		                    <Grid item
		                        xs={12} sm={4} md={4} lg={4}>
		                        <Card>
                                  <CardActionArea>
                                    <CardContent>
                                      <Typography gutterBottom variant="h6" component="h2"
                                        style={{ fontVariant: 'small-caps' }}>
                                        5 ml algorithms
                                      </Typography>
                                      <Typography component="p" style={styles.spanCard}>
                                        Those 5 classification algorithms in action for a complete result
                                      </Typography>
                                    </CardContent>
                                  </CardActionArea>
                                </Card>
		                    </Grid>
		                    <Grid item
		                        xs={12} sm={4} md={4} lg={4}>
		                        <Card>
                                  <CardActionArea>
                                    <CardContent>
                                      <Typography gutterBottom variant="h6" component="h2"
                                        style={{ fontVariant: 'small-caps' }}>
                                        +10k models
                                      </Typography>
                                      <Typography component="p" style={styles.spanCard}>
                                        A huge amount of hyperparameters to find the most effective model
                                      </Typography>
                                    </CardContent>
                                  </CardActionArea>
                                </Card>
		                    </Grid>
		                </Grid>

                        <div className="subtitle-page">Backtest features</div>
		                <Grid container
		                    spacing={8}
		                    style={{ marginBottom : 30 }}>
		                    <Grid item
		                        xs={12} sm={4} md={4} lg={4}>
		                        <Card>
                                  <CardActionArea>
                                    <CardContent>
                                      <Typography gutterBottom variant="h6" component="h2"
                                        style={{ fontVariant: 'small-caps' }}>
                                        No look-ahead bias
                                      </Typography>
                                      <Typography component="p" style={styles.spanCard}>
                                        The backtest algorithm makes sure to not use unavailable future data
                                      </Typography>
                                    </CardContent>
                                  </CardActionArea>
                                </Card>
		                    </Grid>
		                    <Grid item
		                        xs={12} sm={4} md={4} lg={4}>
		                        <Card>
                                  <CardActionArea>
                                    <CardContent>
                                      <Typography gutterBottom variant="h6" component="h2"
                                        style={{ fontVariant: 'small-caps' }}>
                                        Transaction cost
                                      </Typography>
                                      <Typography component="p" style={styles.spanCard}>
                                        Possibility to simulate the transaction cost
                                      </Typography>
                                    </CardContent>
                                  </CardActionArea>
                                </Card>
		                    </Grid>
		                    <Grid item
		                        xs={12} sm={4} md={4} lg={4}>
		                        <Card>
                                  <CardActionArea>
                                    <CardContent>
                                      <Typography gutterBottom variant="h6" component="h2"
                                        style={{ fontVariant: 'small-caps' }}>
                                        Perf. measurement
                                      </Typography>
                                      <Typography component="p" style={styles.spanCard}>
                                        Analyse the backtest results with relevant charts and metrics
                                      </Typography>
                                    </CardContent>
                                  </CardActionArea>
                                </Card>
		                    </Grid>
		                </Grid>
                    </Grid>
                </Grid>
            </div>
        </div>
		)
	}
}
