import React, {Component}  from 'react';
import PropTypes from "prop-types";

import AppLoading from './AppLoading.js';

import { scaleTime } from "d3-scale";
import { utcDay } from "d3-time";
import { format } from "d3-format";
import { timeFormat } from "d3-time-format";

import { ChartCanvas, Chart } from "react-stockcharts";
import { XAxis, YAxis } from "react-stockcharts/lib/axes";
import { fitWidth } from "react-stockcharts/lib/helper";
import { last, timeIntervalBarWidth } from "react-stockcharts/lib/utils";
import {
	BarSeries,
	CandlestickSeries,
	LineSeries,
} from "react-stockcharts/lib/series";
import {
	CrossHairCursor,
	EdgeIndicator,
	CurrentCoordinate,
	MouseCoordinateX,
	MouseCoordinateY,
} from "react-stockcharts/lib/coordinates";
import { discontinuousTimeScaleProvider } from "react-stockcharts/lib/scale";

export default class AppCandleStickChart extends Component {

    constructor(props) {
        super(props);
        this.state = { data: [] };
    }

    componentDidMount() {
    }

  render() {
        var { data: initialData, width, height, interval } = this.props;

        if (initialData == null || initialData.length == 0 ) {
            return (<AppLoading/>);
        }

        if (typeof initialData[0].date === 'string') {
            for (var e in initialData) {
                initialData[e].date = new Date(initialData[e].date);
            }
        }

        console.log('AA')
        console.log(initialData)

        var xScaleProvider = discontinuousTimeScaleProvider.inputDateAccessor(d => d.date);
		const {
			data,
			xScale,
			xAccessor,
			displayXAccessor,
		} = xScaleProvider(initialData);

		const start = xAccessor(last(data));
		const end = xAccessor(data[Math.max(0, data.length - 100)]);
		const xExtents = [start, end];

		const margin = { left: 50, right: 50, top: 10, bottom: 30 };
		const gridHeight = height - margin.top - margin.bottom;
		const gridWidth = width - margin.left - margin.right;

		const showGrid = true;
        const yGrid = showGrid ? { innerTickSize: -1 * gridWidth, tickStrokeOpacity: 0.2 } : {};

		return (
			<ChartCanvas
			        height={height}
					ratio={1}
					width={width}
					margin={margin}
					seriesName="AAAP"
					data={data}
                   xAccessor={xAccessor}
					xScale={scaleTime()}
					xExtents={xExtents}
				    >

				<Chart id={1} yExtents={d => [d.high, d.low]} height={gridHeight}>
					<XAxis axisAt="bottom" orient="bottom" ticks={6}
					stroke="#E0E0E0" tickStroke="#E0E0E0" opacity={0.5}/>
					<YAxis axisAt="right" orient="right" ticks={5}
					stroke="#E0E0E0" tickStroke="#E0E0E0" opacity={0.5} {...yGrid}/>
					<CandlestickSeries
						stroke={d => d.close > d.open ? "#00bcd4" : "#ff4081"}
						wickStroke={d => d.close > d.open ? "#00bcd4" : "#ff4081"}
						fill={d => d.close > d.open ? "#00bcd4" : "#ff4081"} />
					<MouseCoordinateY
						at="right"
						orient="right"
						displayFormat={format(".2f")} />
					<MouseCoordinateX
						at="bottom"
						orient="bottom"
						displayFormat={timeFormat("%Y-%m-%d")} />
					<EdgeIndicator itemType="last" orient="right" edgeAt="right"
						yAccessor={d => d.close} fill={d => d.close > d.open ? "#00bcd4" : "#ff4081"}/>
				</Chart>
				<Chart id={2}
						yExtents={d => d.volume}
						height={gridHeight/5} origin={(w, h) => [0, gridHeight/5*4]} >
					<YAxis axisAt="left" orient="left" ticks={5} tickFormat={format(".0s")}
							tickStroke="#E0E0E0"/>
					<BarSeries
							yAccessor={d => d.volume}
							fill={d => d.close > d.open ? "#00bcd4" : "#ff4081"} />
				</Chart>
				<CrossHairCursor stroke="#FFFFFF" />
			</ChartCanvas>
		);
    }
};

AppCandleStickChart.propTypes = {
    interval: PropTypes.oneOf(["D", "W", "M"]),
	data: PropTypes.array.isRequired,
	width: PropTypes.number.isRequired,
	height: PropTypes.number.isRequired
};

AppCandleStickChart.defaultProps = {
	type: "svg",
	interval: "D",
};