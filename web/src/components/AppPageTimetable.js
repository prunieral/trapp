import React, {Component}  from 'react';
import ReactDOM from 'react-dom';
import { contains, getData } from "../utils.js";
import { TypeChooser } from "react-stockcharts/lib/helper";
import { connect } from "react-redux";
import axios from 'axios';
import ContainerDimensions from 'react-container-dimensions';
import AceEditor from 'react-ace';
import AppWorldMap from './AppWorldMap.js';

@connect((store) => {
    return {
        p : store.pagetimetable,
        global : store.global
    }
})
export default class AppPageTimetable extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        axios.get('getSchedule').then(response => {
            this.props.dispatch({type: "TIMETABLE_SET_SCHEDULE", payload: response.data});
            this.setState({});
        }).catch((error) => {
            console.log(error);
        });
	}

    render() {
		return (
		<div className="page">
		    <div className="title-page">Timetable</div>
		    <div className="pageTimetable-Body">
                <ContainerDimensions>
                  { ({ width, height }) =>
                  <AppWorldMap
                    height={height}
                    width={width}
                    data={this.props.p.schedule}
                  />
                    }
                </ContainerDimensions>
            </div>
        </div>
		)
	}
}