import React, {Component}  from 'react';
import ReactDOM from 'react-dom';
import AppCandleStickChart from './AppCandleStickChart.js';
import { contains, getData } from "../utils.js";
import { TypeChooser } from "react-stockcharts/lib/helper";
import Select from '@material-ui/core/Select';
import { connect } from "react-redux";
import axios from 'axios';
import ContainerDimensions from 'react-container-dimensions';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';

@connect((store) => {
    return {
        consult : store.pageconsult,
        global : store.global

    }
})
export default class AppPageConsult extends Component {

    constructor(props) {
        super(props);
        this.state = {}
        this.handleChangeStockExchange = this.handleChangeStockExchange.bind(this);
        this.handleChangeSecurity = this.handleChangeSecurity.bind(this);
    }

    handleChangeStockExchange(event) {
        this.props.dispatch({type: "CHANGE_STOCKEXCHANGE", payload: event.target.value});
        if (contains(this.props.consult.stockExchanges, event.target.value) > -1) {
            axios.get("getSecurities?stockexchange=" + event.target.value).then(response => {
                this.props.dispatch({type: "SET_SECURITIES", payload: response.data});
            }).catch((error) => {
                console.log(error)
            });
        }
    }

    handleChangeSecurity(event) {
        this.props.dispatch({type: "CHANGE_SECURITY", payload: event.target.value});
        if (contains(this.props.consult.stockExchanges, this.props.consult.stockExchange) > -1 &&
            contains(this.props.consult.securities, event.target.value) > -1) {
            getData(this.props.consult.stockExchange, event.target.value).then(data => {
			    this.props.dispatch({type: "SET_DATA", payload : data })
		    })
		}
    }

    componentDidMount() {
        axios.get('getStockExchanges').then(response => {
            this.props.dispatch({type: "SET_STOCKEXCHANGES", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
	}

    render() {
		return (
		<div className="page">
		    <div className="title-page">Consulting the stocks</div>
		    <div id="pageConsult-Header">
		        <Grid container>
                    <Grid item xs={12}
                        style={{ paddingLeft: 53 }}>
                        <FormControl
                            style={{ minWidth: 200 }}>
                        <InputLabel htmlFor="age-native-simple">Stock Exchange</InputLabel>
                        <Select
                          value={this.props.consult.stockExchange}
                          onChange={this.handleChangeStockExchange.bind(this)}>
                              {this.props.consult.stockExchanges.map(function(exchange){
                                return <MenuItem value={exchange}>{exchange}</MenuItem>;
                              })}
                        </Select>
                        </FormControl>
                        <FormControl
                            style={{minWidth: 200}}>
                        <InputLabel htmlFor="age-native-simple">Security</InputLabel>
                        <Select
                          value={this.props.consult.security}
                          onChange={this.handleChangeSecurity.bind(this)}>
                              {this.props.consult.securities.map(function(security){
                                return <MenuItem value={security}>{security}</MenuItem>;
                              })}
                        </Select>
                        </FormControl>
                    </Grid>
                </Grid>
            </div>
            <div id="pageConsult-Body">
                <ContainerDimensions>
                    { ({ width }) =>
                    <AppCandleStickChart
                        data={this.props.consult.data}
                        width={width}
                        height={this.props.global.size.height-70}/>
                    }
                </ContainerDimensions>
            </div>*/
        </div>
		)
	}
}