import React, {Component}  from 'react';
import Paper from '@material-ui/core/Paper'
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import { stack as Menu } from 'react-burger-menu';
import Icon from '@material-ui/core/Icon';
import { connect } from 'react-redux';
import HomeIcon from '@material-ui/icons/Home';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import ViewQuiltIcon from '@material-ui/icons/ViewQuilt';
import BuildIcon from '@material-ui/icons/Build';
import GestureIcon from '@material-ui/icons/Gesture';
import NetworkCheckIcon from '@material-ui/icons/NetworkCheck';
import ScheduleIcon from '@material-ui/icons/Schedule';

@connect((store) => {
    return {
        global : store.global
    }
})
export default class AppMenu extends Component {

    setHomePage() {
        this.props.dispatch({type: "SET_HOME_PAGE"});
    }

    setConsultationPage() {
        this.props.dispatch({type: "SET_CONSULTATION_PAGE"});
    }

    setDashboardPage() {
        this.props.dispatch({type: "SET_DASHBOARD_PAGE"});
    }

    setAssessmentPage() {
        this.props.dispatch({type: "SET_ASSESSMENT_PAGE"});
    }

    setBacktestPage() {
        this.props.dispatch({type: "SET_BACKTEST_PAGE"});
    }

    setSettingsPage() {
        this.props.dispatch({type: "SET_SETTINGS_PAGE"});
    }

    setTimetablePage() {
        this.props.dispatch({type: "SET_TIMETABLE_PAGE"});
    }

  render() {

        var styles = {
            shortMenu : {
                position: 'fixed',
                top: 80,
                left: 20,
                width: 30,
                marginBottom: 10,
            },
            menuSpace: {
                width: 30,
                height: 30
            }
        }

        return (
        <div>
            <div style={styles.shortMenu}>
                <a id="home"
                    className={this.props.global.activePage == 'home' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setHomePage.bind(this)}>
                    <HomeIcon/>
                </a>
                <div style={styles.menuSpace}/>
                <a id="consultation"
                    className={this.props.global.activePage == 'consultation' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setConsultationPage.bind(this)}>
                    <ShowChartIcon/>
                </a>
                <a id="dashboard"
                    className={this.props.global.activePage == 'dashboard' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setDashboardPage.bind(this)}>
                    <ViewQuiltIcon/>
                </a>
                <a id="timetable"
                    className={this.props.global.activePage == 'timetable' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setTimetablePage.bind(this)}>
                    <ScheduleIcon/>
                </a>
                <div style={styles.menuSpace}/>
                <a id="assessment"
                    className={this.props.global.activePage == 'assessment' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setAssessmentPage.bind(this)}>
                    <GestureIcon/>
                </a>
                <a id="backtest"
                    className={this.props.global.activePage == 'backtest' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setBacktestPage.bind(this)}>
                    <NetworkCheckIcon/>
                </a>
                <div style={styles.menuSpace}/>
                <a id="settings"
                    className={this.props.global.activePage == 'settings' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setSettingsPage.bind(this)}>
                    <BuildIcon/>
                </a>
            </div>
            <Menu width={ 400 } left outerContainerId={ "app" } id={'full-menu'}>
                <a id="home"
                    className={this.props.global.activePage == 'home' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setHomePage.bind(this)}>
                    <HomeIcon/>
                    <span className="menu-title">Home</span>
                </a>
                <br/>
                <a id="consultation"
                    className={this.props.global.activePage == 'consultation' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setConsultationPage.bind(this)}>
                    <ShowChartIcon/>
                    <span className="menu-title">Consultation</span>
                </a>
                <a id="dashboard"
                    className={this.props.global.activePage == 'dashboard' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setDashboardPage.bind(this)}>
                    <ViewQuiltIcon/>
                    <span className="menu-title">Dashboard</span>
                </a>
                <a id="timetable"
                    className={this.props.global.activePage == 'timetable' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setTimetablePage.bind(this)}>
                    <ScheduleIcon/>
                    <span className="menu-title">Timetable</span>
                </a>
                <br/>
                <a id="assessment"
                    className={this.props.global.activePage == 'assessment' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setAssessmentPage.bind(this)}>
                    <GestureIcon/>
                    <span className="menu-title">Model Assessment</span>
                </a>
                <a id="backtest"
                    className={this.props.global.activePage == 'backtest' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setBacktestPage.bind(this)}>
                    <NetworkCheckIcon/>
                    <span className="menu-title">Backtest</span>
                </a>
                <br/>
                <a id="settings"
                    className={this.props.global.activePage == 'settings' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setSettingsPage.bind(this)}>
                    <BuildIcon/>
                    <span className="menu-title">Settings</span>
                </a>
            </Menu>
        </div>);
    }
}