import React, {Component}  from 'react';
import ReactDOM from 'react-dom';
import { contains, getData } from "../utils.js";
import { TypeChooser } from "react-stockcharts/lib/helper";
import Grid from '@material-ui/core/Grid';
import { connect } from "react-redux";
import axios from 'axios';
import ContainerDimensions from 'react-container-dimensions';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DoneIcon from '@material-ui/icons/Done';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import NetworkCheckIcon from '@material-ui/icons/NetworkCheck';
import Avatar from '@material-ui/core/Avatar';
import Checkbox from '@material-ui/core/Checkbox';
import EnhancedTable from './AppTable.js';
import AppBubbleChart from './AppBubbleChart.js';
import ReactGridLayout from 'react-grid-layout';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';

@connect((store) => {
    return {
        assessment : store.pageassessment,
        global : store.global
    }
})
export default class AppPageAssessment extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        axios.get('getAssessments').then(response => {
            this.props.dispatch({type: "ASSESSMENT_SET_ASSESSMENTS",
                payload: JSON.parse(response.data.replace(/'/g, '"').replace(/\s/g, ''))});
        }).catch((error) => {
            console.log(error)
        });
	}

	onClickAssessment(item) {
	    this.props.dispatch({type: "ASSESSMENT_SET_SELECTED_ASSESSMENT", payload: item});
	    axios.get('getAssessmentDetails?assessment=' + item).then(response => {
            this.props.dispatch({type: "ASSESSMENT_SET_ASSESSMENT_DETAIL",
                payload: JSON.parse(response.data.replace(/'/g, '"').replace(/\s/g, ''))});
        }).catch((error) => {
            console.log(error)
        });
	}

	onResizeStop(e) {
	    this.props.dispatch({type: "ASSESSMENT_SAVE_STATE", payload: e});
        this.setState({});
    }

    onDragStop(e) {
	    this.props.dispatch({type: "ASSESSMENT_SAVE_STATE", payload: e});
        this.setState({});
    }

    handlePress(e) {
        e.stopPropagation();
    }

    onModelClick(id) {
        var selectedModel = this.props.assessment.assessmentDetail.filter(d => d.id === id)[0];
        selectedModel['assessment'] = this.props.assessment.selectedAssessment;
        this.props.dispatch({type: "SET_BACKTEST_PAGE", payload: null});
        this.props.dispatch({type: "BACKTEST_SET_MODEL", payload: selectedModel});
	}

    render() {

        var styles = {
          listAssessments: {
                overflow: 'auto',
                maxHeight: this.props.global.size.height - 25,
                position: 'fixed',
          },
          listElement: {
                padding: 0,
          },
          listElementText: {
                paddingRight: '10px',
                paddingLeft: '5px',
                fontSize: 12,
          },
          avatar: {
                transform: 'scale(0.6)',
          },
          toolbar: {
                background: "black",
                height: 35,
                minHeight: 35,
                padding: "0px 5px"
          },
        }

        var order = null;
        var orderBy = null;

		return (
		<div className="page">
		    <div className="title-page">Model Assessment</div>
		    <Grid container spacing={16}>
                <Grid item xs={4} sm={2}>
                  <List
                        style={styles.listAssessments}>
                      {this.props.assessment.assessments.map((a) => (
                        <ListItem button
                            key={a.name}
                            style={styles.listElement}
                            selected={a.name == this.props.assessment.selectedAssessment}
                            onClick={this.onClickAssessment.bind(this, a.name)}>
                            <ListItemAvatar
                                style={styles.avatar}>
                              <Avatar>
                                {(((new Date()).getTime() - new Date(a.date).getTime()) / (1000 * 60 * 60 * 24)) < 7 ?
                                    <DoneIcon /> : <AccessTimeIcon />}
                              </Avatar>
                            </ListItemAvatar>
                            <ListItemText
                                style={styles.listElementText}
                                primary={a.name} />
                        </ListItem>
                      ))}
                    </List>
                </Grid>
                <Grid item xs={8} sm={10}>
                    <ContainerDimensions>
                      { ({ width, height }) =>
                      <EnhancedTable
                        name={this.props.assessment.selectedAssessment}
                        data={this.props.assessment.assessmentDetail}
                        width={width}
                        selection={false}
                        action1Button={true}
                        onAction1Click={this.onModelClick.bind(this)}
                        iconAction1={<NetworkCheckIcon/>}
                      />
                        }
                    </ContainerDimensions>
                    <ContainerDimensions>
                      { ({ width, height }) =>
                        <ReactGridLayout {...this.props}
                            className="layout"
                            layout={this.props.assessment.layout}
                            onResizeStop={this.onResizeStop.bind(this)}
                            onDragStop={this.onDragStop.bind(this)}
                            cols={16}
                            rowHeight={80}
                            width={width}>
                                <div key={5} className={"chart-5"}
                                    data-grid={{draggableHandle: '.draggable', x: 0, y: 0, w: 8, h: 4, minW: 0, minH: 0}}>
                                    <div className="draggable">
                                        <AppBar
                                            position="static"
                                            color="default"
                                            style={styles.toolbar}>
                                            <Toolbar
                                                style={styles.toolbar}
                                                variant="dense">
                                                <div className="dashboard-title">
                                                    Accuracy mean/std
                                                </div>
                                            </Toolbar>
                                        </AppBar>
                                    </div>
                                    <div className={"pageDashboard-ElBody"} ref="chartArea"
                                        onMouseDown={e => this.handlePress(e)}>
                                        <ContainerDimensions>
                                            { ({ width, height }) =>
                                            <AppBubbleChart
                                                data={this.props.assessment.assessmentDetail}
                                                width={width}
                                                height={height}
                                                x={"mean_test_accuracy"}
                                                y={"std_test_accuracy"}/>
                                            }
                                        </ContainerDimensions>
                                    </div>
                                </div>
                                <div key={2} className={"chart-2"}
                                    data-grid={{draggableHandle: '.draggable', x: 8, y: 0, w: 8, h: 4, minW: 0, minH: 0}}>
                                    <div className="draggable">
                                        <AppBar
                                            position="static"
                                            color="default"
                                            style={styles.toolbar}>
                                            <Toolbar
                                                style={styles.toolbar}
                                                variant="dense">
                                                <div className="dashboard-title">
                                                    ROC AUC mean/std
                                                </div>
                                            </Toolbar>
                                        </AppBar>
                                    </div>
                                    <div className={"pageDashboard-ElBody"} ref="chartArea"
                                        onMouseDown={e => this.handlePress(e)}>
                                        <ContainerDimensions>
                                            { ({ width, height }) =>
                                            <AppBubbleChart
                                                data={this.props.assessment.assessmentDetail}
                                                width={width}
                                                height={height}
                                                x={"mean_test_roc_auc"}
                                                y={"std_test_roc_auc"}/>
                                            }
                                        </ContainerDimensions>
                                    </div>
                                </div>
                                <div key={3} className={"chart-3"}
                                    data-grid={{draggableHandle: '.draggable', x: 0, y: 1, w: 8, h: 4, minW: 0, minH: 0}}>
                                    <div className="draggable">
                                        <AppBar
                                            position="static"
                                            color="default"
                                            style={styles.toolbar}>
                                            <Toolbar
                                                style={styles.toolbar}
                                                variant="dense">
                                                <div className="dashboard-title">
                                                    F1 mean/std
                                                </div>
                                            </Toolbar>
                                        </AppBar>
                                    </div>
                                    <div className={"pageDashboard-ElBody"} ref="chartArea"
                                        onMouseDown={e => this.handlePress(e)}>
                                        <ContainerDimensions>
                                            { ({ width, height }) =>
                                            <AppBubbleChart
                                                data={this.props.assessment.assessmentDetail}
                                                width={width}
                                                height={height}
                                                x={"mean_test_f1"}
                                                y={"std_test_f1"}/>
                                            }
                                        </ContainerDimensions>
                                    </div>
                                </div>
                                <div key={4} className={"chart-4"}
                                    data-grid={{draggableHandle: '.draggable', x: 8, y: 1, w: 8, h: 4, minW: 0, minH: 0}}>
                                    <div className="draggable">
                                        <AppBar
                                            position="static"
                                            color="default"
                                            style={styles.toolbar}>
                                            <Toolbar
                                                style={styles.toolbar}
                                                variant="dense">
                                                <div className="dashboard-title">
                                                    Neg log loss mean/std
                                                </div>
                                            </Toolbar>
                                        </AppBar>
                                    </div>
                                    <div className={"pageDashboard-ElBody"} ref="chartArea"
                                        onMouseDown={e => this.handlePress(e)}>
                                        <ContainerDimensions>
                                            { ({ width, height }) =>
                                            <AppBubbleChart
                                                data={this.props.assessment.assessmentDetail}
                                                width={width}
                                                height={height}
                                                x={"mean_test_neg_log_loss"}
                                                y={"std_test_neg_log_loss"}/>
                                            }
                                        </ContainerDimensions>
                                    </div>
                                </div>
                        </ReactGridLayout>
                      }
                    </ContainerDimensions>
                </Grid>
            </Grid>
        </div>
		)
	}
}