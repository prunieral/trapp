import { csvParse } from  "d3-dsv";
import { timeParse } from "d3-time-format";
import * as fs from 'fs';

function parseData(parse) {
	return function(d) {
		d.date = parse(d.Date);
		d.open = +d.Open;
		d.high = +d.High;
		d.low = +d.Low;
		d.close = +d.Close;
		d.volume = +d.Volume;

		return d;
	};
}

const parseDate = timeParse("%Y-%m-%d");

export function getData(value1, value2) {
	const promiseMSFT = fetch(value1 + "/" + value2 + ".csv")
		.then(response => response.text())
		.then(data => csvParse(data, parseData(parseDate)))
	return promiseMSFT;
}

export function contains(array, word) {
    var indexOf = -1;

    for(var i = 0; i < array.length; i++) {
        if(array[i] == word) {
            return i;
        }
    }

    return indexOf;
};