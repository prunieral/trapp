import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store, persistor } from "./store";
import App from './components/App.js';
import { CookiesProvider } from 'react-cookie';
import { PersistGate } from 'redux-persist/integration/react';

var app = document.getElementById("app");

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <CookiesProvider>
                <App
                    persistor={persistor}/>
            </CookiesProvider>
        </PersistGate>
    </Provider>
    , app);


