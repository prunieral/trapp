import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import reducers from "./redux/Combiner";
import storage from 'redux-persist/lib/storage'
import { persistStore, persistReducer } from 'redux-persist';

const middleware = applyMiddleware(promise(), thunk, createLogger());

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, reducers)

var tmp_store = createStore(persistedReducer, undefined, ...middleware)
var tmp_persistor = persistStore(tmp_store)

export const store = tmp_store
export const persistor = tmp_persistor