
rem Set up the installation

set SESSION=scp://ubuntu@onet.anthonyhivert.ovh:2222/
set REMOTE_PATH=/var/www/html/trapp/
cd ..
set MY_PATH=%cd%
rmdir /s /q "%MY_PATH%\web\node_modules"
cd bin

rem upload files

scp -P 2222 -r "%MY_PATH%/bin" %SESSION%/var/www/html/trapp/
scp -P 2222 -r "%MY_PATH%/data" %SESSION%/var/www/html/trapp/
scp -P 2222 -r "%MY_PATH%/doc" %SESSION%/var/www/html/trapp/
scp -P 2222 -r "%MY_PATH%/python" %SESSION%/var/www/html/trapp/
scp -P 2222 -r "%MY_PATH%/web" %SESSION%/var/www/html/trapp/

rem reinstall node modules

cd ../web
npm install

rem reinstall node modules on the production server

rem TODO

rem prepare exit

set RESULT=%ERRORLEVEL%
rem Propagating WinSCP exit code
exit /b %RESULT%

pause