import sys
from python.tool.DataReader import DataReader
from python.tool.ColumnBuilder import ColumnBuilder
import matplotlib as plt
import matplotlib

matplotlib.use('TkAgg')

sys.path.append('..')

# PREPARE DATAFRAME

dataReader = DataReader()
stocks = dataReader.get_stocks()
dfs = []

for exchange in stocks.keys():
    for stock in stocks[exchange]:
        df = dataReader.read_specific_file(exchange, stock)
        df = ColumnBuilder.build_bollinger_bands(df)
        df = df.drop(columns=['Open', 'High', 'Low', 'Volume', 'Turnover'])
        dfs.append(df)

dfs[0].plot(kind='line')

plt.pyplot.show()
plt.interactive(False)

print(dfs[0].index[-1])