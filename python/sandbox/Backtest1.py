from CryptoLinearBacktest import CryptoLinearBacktest
import numpy as np

cols1 = {'build_std' : { 'past_time' : 5 }}
cols2 = {'build_ewma' : { 'past_time' : 5, 'ref_col' : 'close' }}

date = '20180715'

for min_accuracy in np.arange(0.6, 1, 0.1):
    for span in range(2, 10, 2):
        CryptoLinearBacktest.test(date,
                                  cols1,
                                  0.05,
                                  min_accuracy,
                                  span,
                                  0.001,
                                  verbose=False,
                                  save_result=True)

for min_accuracy in np.arange(0.6, 1, 0.1):
    for span in range(2, 10, 2):
        CryptoLinearBacktest.test(date,
                                  cols2,
                                  0.05,
                                  min_accuracy,
                                  span,
                                  0.001,
                                  verbose=False,
                                  save_result=True)


