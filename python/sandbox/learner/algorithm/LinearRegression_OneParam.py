import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np

class LinearRegression_OneParam:

    trainingSet = None
    m = None

    def __init__(self):
        None

    def set_training_set(self, trainingSet):
        self.trainingSet = trainingSet
        self.m = len(self.trainingSet)

    def h(self, x, theta0, theta1):
        return theta0 + theta1 * x

    def cost_function(self, theta0, theta1):

        #Getting first part of the cost function
        firstPart = 1 / ( 2 * self.m )

        #Calculating second part of the cost function
        secondPart = 0
        for index, row in self.trainingSet.iterrows():
            secondPart += ((theta0 + row.Size * theta1) - row.Price) ** 2

        #Multiplying the first part by the second one
        result = firstPart * secondPart

        return result

    def find_best_cost_function_with_gradient_descent(self):

        last_cost = None
        theta0 = 0.0
        theta1 = 1.0
        alpha = 1.0

        while True:

            #Calculating the costFunction with the current thetas
            new_cost = self.cost_function(theta0, theta1)

            print(str(alpha) + ' - ' + str(last_cost) + ' - ' + str(new_cost) + ' - ' + str(theta0) + ' - ' + str(theta1))

            #Decrease alpha if it is not converging
            if last_cost is None:
                tempTheta0 = theta0 - alpha * (1 / self.m) * sum((self.h(row.Size, theta0, theta1) - row.Price) for index, row in self.trainingSet.iterrows())
                tempTheta1 = theta1 - alpha * (1 / self.m) * sum((self.h(row.Size, theta0, theta1) - row.Price) * row.Size for index, row in self.trainingSet.iterrows())
                future_cost = self.cost_function(tempTheta0, tempTheta1)
                if future_cost > new_cost:
                    alpha *= 0.99 #0.33 was advised but make the gradient descent too long
                    continue

            #return thetas if the best values are found with an acceptable cost
            if last_cost is not None and ( ( last_cost / new_cost ) - 1 ) < 0.01 :
                return [theta0, theta1]

            #Calculate new thetas
            tempTheta0 = theta0 - alpha * ( 1 / self.m ) * sum( (self.h(row.Size, theta0, theta1) - row.Price ) for index, row in self.trainingSet.iterrows() )
            tempTheta1 = theta1 - alpha * ( 1 / self.m ) * sum( (self.h(row.Size, theta0, theta1) - row.Price ) * row.Size for index, row in self.trainingSet.iterrows() )
            theta0 = tempTheta0
            theta1 = tempTheta1

            last_cost = new_cost

    def find_best_cost_function_with_normal_equation(self):

        X = self.trainingSet.ix[:, ['Size']]
        X.insert(loc=0, column='x0', value=1)

        y = self.trainingSet.ix[:, ['Price']]

        theta = np.dot ( np.dot( np.linalg.pinv( np.dot( X.transpose(), X ) ), X.transpose() ) , y)

        return [theta[0][0], theta[1][0]]


#Init

currentDir = os.path.dirname(os.path.realpath(__file__))

#Learning

ph_linear_regression = LinearRegression_OneParam()
ph_linear_regression.set_training_set(pd.read_csv(os.path.join(currentDir, 'data', 'PerfectHousingData.csv'), parse_dates=False))
ph_result = ph_linear_regression.find_best_cost_function_with_gradient_descent()
ne_ph_result = ph_linear_regression.find_best_cost_function_with_normal_equation()

h_linear_regression = LinearRegression_OneParam()
h_linear_regression.set_training_set(pd.read_csv(os.path.join(currentDir, 'data', 'HousingData.csv'), parse_dates=False))
h_result = h_linear_regression.find_best_cost_function_with_gradient_descent()
ne_h_result = h_linear_regression.find_best_cost_function_with_normal_equation()

#Plotting

ph_linear_regression.trainingSet.set_index('Size')['Price']
ph_linear_regression.trainingSet['Linear'] = ph_linear_regression.h(ph_linear_regression.trainingSet['Size'], ph_result[0], ph_result[1])
ax = ph_linear_regression.trainingSet[['Size', 'Price']].plot(x='Size', style='o')
ph_linear_regression.trainingSet[['Size', 'Linear']].plot(x='Size', ax=ax, title='PerfectHousingData - Gradient Descent')

h_linear_regression.trainingSet.set_index('Size')['Price']
h_linear_regression.trainingSet['Linear'] = h_linear_regression.h(h_linear_regression.trainingSet['Size'], h_result[0], h_result[1])
ax = h_linear_regression.trainingSet[['Size', 'Price']].plot(x='Size', style='o')
h_linear_regression.trainingSet[['Size', 'Linear']].plot(x='Size', ax=ax, title='HousingData - Gradient Descent')

ph_linear_regression.trainingSet.set_index('Size')['Price']
ph_linear_regression.trainingSet['Linear_NE'] = ph_linear_regression.h(ph_linear_regression.trainingSet['Size'], ne_ph_result[0], ne_ph_result[1])
ax = ph_linear_regression.trainingSet[['Size', 'Price']].plot(x='Size', style='o')
ph_linear_regression.trainingSet[['Size', 'Linear_NE']].plot(x='Size', ax=ax, title='PerfectHousingData - Normal Equation')

h_linear_regression.trainingSet.set_index('Size')['Price']
h_linear_regression.trainingSet['Linear_NE'] = h_linear_regression.h(h_linear_regression.trainingSet['Size'], ne_h_result[0], ne_h_result[1])
ax = h_linear_regression.trainingSet[['Size', 'Price']].plot(x='Size', style='o')
h_linear_regression.trainingSet[['Size', 'Linear_NE']].plot(x='Size', ax=ax, title='HousingData - Normal Equation')

plt.show()

