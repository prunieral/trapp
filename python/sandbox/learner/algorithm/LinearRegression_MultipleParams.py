import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.cm as cmx
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

class LinearRegression_MultipleParams:

    trainingSet = None
    m = None

    def __init__(self):
        None

    def set_training_set(self, trainingSet):
        self.trainingSet = trainingSet
        self.m = len(self.trainingSet)

    def h(self, x, theta):
        result = theta['Theta'][0]
        for i in range(len(x)):
            result += theta['Theta'][i+1] * x[i]
        return result

    def cost_function(self, theta):

        #Getting first part of the cost function
        firstPart = 1 / ( 2 * self.m )

        #Calculating second part of the cost function
        secondPart = 0
        for index, row in self.trainingSet.iterrows():
            secondPart += (self.h(row.iloc[:-1], theta) - row.Price) ** 2

        #Multiplying the first part by the second one
        result = firstPart * secondPart

        return result

    def find_best_cost_function_with_gradient_descent(self):

        last_cost = None
        theta = pd.DataFrame(data={'Theta':[1 for i in range(len(self.trainingSet.columns))]})
        alpha = 1.0

        while True:

            #Calculating the costFunction with the current thetas
            new_cost = self.cost_function(theta)

            print(str(alpha) + ' - ' + str(last_cost) + ' - ' + str(new_cost) )#+ ' - ' + str(theta))

            #Decrease alpha if it is not converging
            if last_cost is None:
                tempTheta = pd.DataFrame(data={'Theta':[1 for i in range(len(self.trainingSet.columns))]})
                for i, tx in tempTheta['Theta'].iteritems():
                    tempTheta['Theta'][i] = theta['Theta'][i] - alpha * (1 / self.m) * sum((self.h(row.iloc[:-1], theta) - row.Price) * (row[i-1] if i > 0 else 1) for index, row in self.trainingSet.iterrows())
                
                futureCost = self.cost_function(tempTheta)
                
                if futureCost > new_cost:
                    alpha *= 0.99 #0.33 was advised but make the gradient descent too long
                    continue

            #return thetas if the best values are found with an acceptable cost
            if last_cost is not None and ( ( last_cost / new_cost ) - 1 ) < 0.01 :
                return theta

            #Calculate new thetas
            tempTheta = pd.DataFrame(data={'Theta':[1 for i in range(len(self.trainingSet.columns))]})
            for i, tx in tempTheta['Theta'].iteritems():
                tempTheta['Theta'][i] = theta['Theta'][i] - alpha * (1 / self.m) * sum((self.h(row.iloc[:-1], theta) - row.Price) * (row[i - 1] if i > 0 else 1) for index, row in self.trainingSet.iterrows())
            theta = tempTheta

            last_cost = new_cost

    def find_best_cost_function_with_normal_equation(self):

        X = self.trainingSet.drop('Price', axis=1)
        X.insert(loc=0, column='x0', value=1)

        y = self.trainingSet.ix[:, ['Price']]

        result = np.dot ( np.dot( np.linalg.pinv( np.dot( X.transpose(), X ) ), X.transpose() ) , y)

        return pd.DataFrame([theta[0] for theta in result])


#Init

currentDir = os.path.dirname(os.path.realpath(__file__))

#Learning

h_linear_regression = LinearRegression_MultipleParams()
h_linear_regression.set_training_set(pd.read_csv(os.path.join(currentDir, 'data', 'MultipleHousingData.csv'), parse_dates=False))
theta = h_linear_regression.find_best_cost_function_with_gradient_descent()
theta_ne = h_linear_regression.find_best_cost_function_with_normal_equation()

h_linear_regression.trainingSet['Price_gd'] = h_linear_regression.trainingSet['Price']
h_linear_regression.trainingSet['Price_ne'] = h_linear_regression.trainingSet['Price']

for i, row in h_linear_regression.trainingSet.iterrows():
     row.Price_gd = h_linear_regression.h(row.iloc[:-3], pd.DataFrame([x for x in theta.iloc[:,0]], columns=['Theta']))
for i, row in h_linear_regression.trainingSet.iterrows():
    row.Price_ne = h_linear_regression.h(row.iloc[:-3], pd.DataFrame([x for x in theta_ne.iloc[:,0]], columns=['Theta']))

#Plotting

cm = plt.get_cmap('jet')
cNorm = matplotlib.colors.Normalize(
    vmin=min(h_linear_regression.trainingSet.Price),
    vmax=max(h_linear_regression.trainingSet.Price))
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(h_linear_regression.trainingSet.iloc[:,0],
           h_linear_regression.trainingSet.iloc[:,1],
           h_linear_regression.trainingSet.iloc[:,2],
           c=scalarMap.to_rgba(h_linear_regression.trainingSet.Price))
scalarMap.set_array(h_linear_regression.trainingSet.Price)
fig.colorbar(scalarMap,label='Linear')

#GradientDescent Plotting

cm = plt.get_cmap('jet')
cNorm = matplotlib.colors.Normalize(
    vmin=min(h_linear_regression.trainingSet.Price_gd),
    vmax=max(h_linear_regression.trainingSet.Price_gd))
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(h_linear_regression.trainingSet.iloc[:,0],
           h_linear_regression.trainingSet.iloc[:,1],
           h_linear_regression.trainingSet.iloc[:,2],
           c=scalarMap.to_rgba(h_linear_regression.trainingSet.Price_gd))
scalarMap.set_array(h_linear_regression.trainingSet.Price_gd)
fig.colorbar(scalarMap,label='Linear')

#Normal Equation Plotting

cm = plt.get_cmap('jet')
cNorm = matplotlib.colors.Normalize(
    vmin=min(h_linear_regression.trainingSet.Price_ne),
    vmax=max(h_linear_regression.trainingSet.Price_ne))
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(h_linear_regression.trainingSet.iloc[:,0],
           h_linear_regression.trainingSet.iloc[:,1],
           h_linear_regression.trainingSet.iloc[:,2],
           c=scalarMap.to_rgba(h_linear_regression.trainingSet.Price_ne))
scalarMap.set_array(h_linear_regression.trainingSet.Price_ne)
fig.colorbar(scalarMap,label='Linear')

plt.show()
