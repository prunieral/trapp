import pandas as pd
import os
import matplotlib.pyplot as plt
import pylab
import numpy as np

class LogisticRegression_MultipleParams:

    trainingSet = None
    m = None

    def __init__(self):
        None

    def set_training_set(self, trainingSet):
        self.trainingSet = trainingSet
        self.m = len(self.trainingSet)

    def h(self, x, theta):
        calc = theta['Theta'][0]
        for i in range(len(x)):
            calc += theta['Theta'][i + 1] * x[i]
        return  1 / ( 1 + np.exp( - calc ))

    def cost_function(self, theta):

        # Getting first part of the cost function

        firstPart = 1 / (2 * self.m)

        # Calculating second part of the cost function
        secondPart = 0
        for index, row in self.trainingSet.iterrows():
            secondPart += (self.h(row.iloc[:-1], theta) - row.Exam) ** 2

        # Multiplying the first part by the second one
        result = firstPart * secondPart

        return result

    def find_best_cost_function_with_gradient_descent(self):

        last_cost = None
        theta = pd.DataFrame(data={'Theta': [1 for i in range(len(self.trainingSet.columns))]})
        alpha = 1.0
        i = 0

        while True:

            # Calculating the costFunction with the current thetas
            new_cost = self.cost_function(theta)

            print(str(alpha) + ' - ' + str(last_cost) + ' - ' + str(new_cost) + ' - ' + str(theta))

            # Decrease alpha if it is not converging
            if last_cost is None:
                tempTheta = pd.DataFrame(data={'Theta': [1 for i in range(len(self.trainingSet.columns))]})
                for i, tx in tempTheta['Theta'].iteritems():
                    tempTheta['Theta'][i] = theta['Theta'][i] - alpha * (1 / self.m) * sum((self.h(row.iloc[:-1], theta) - row.Price) * (row[i - 1] if i > 0 else 1) for index, row in self.trainingSet.iterrows())

                future_cost = self.cost_function(tempTheta)
                if future_cost > new_cost:
                    alpha *= 0.99  # 0.33 was advised but make the gradient descent too long
                    continue

            # return thetas if the best values are found with an acceptable cost
            if last_cost is not None and i > 1000:
                return theta

            # Calculate new thetas
            tempTheta = pd.DataFrame(data={'Theta': [1 for i in range(len(self.trainingSet.columns))]})
            for i, tx in tempTheta['Theta'].iteritems():
                tempTheta['Theta'][i] = theta['Theta'][i] - alpha * (1 / self.m) * sum((self.h(row.iloc[:-1], theta) - row.Price) * (row[i - 1] if i > 0 else 1) for index, row in self.trainingSet.iterrows())
            theta = tempTheta

            last_cost = new_cost
            i += 1

#Init

currentDir = os.path.dirname(os.path.realpath(__file__))

#Learning

classification = LogisticRegression_MultipleParams()
classification.set_training_set(pd.read_csv(os.path.join(currentDir, 'data', 'MultipleExamData.csv'), parse_dates=False))
gd_result = classification.find_best_cost_function_with_gradient_descent()

#Plotting

classification.trainingSet.set_index('Grade')['Exam']
ax = classification.trainingSet[['Grade', 'Exam']].plot(x='Grade', style='o', label='Grades & Exam')

x = pylab.linspace(0,20,200)
plt.plot(x, classification.h(x, gd_result[0], gd_result[1]), 'r', label='Gradient Descent')

plt.show()