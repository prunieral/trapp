import pandas as pd
import os
import matplotlib.pyplot as plt
import pylab
import numpy as np

class LogisticRegression_OneParam:

    trainingSet = None
    m = None

    def __init__(self):
        None

    def set_training_set(self, trainingSet):
        self.trainingSet = trainingSet
        self.m = len(self.trainingSet)

    def h(self, x, theta0, theta1):
        return  1 / ( 1 + np.exp( - theta1 * ( theta0 + x )))

    def cost_function(self, theta0, theta1):

        # Getting first part of the cost function
        firstPart = 1 / (2 * self.m)

        # Calculating second part of the cost function
        secondPart = 0
        for index, row in self.trainingSet.iterrows():
            secondPart += (self.h(row.Grade, theta0, theta1) - row.Exam) ** 2

        # Multiplying the first part by the second one
        result = firstPart * secondPart

        return result

    def find_best_cost_function_with_gradient_descent(self):

        last_cost = None
        theta0 = 0.0
        theta1 = 1
        alpha = 1.0
        i = 0

        while True:

            # Calculating the costFunction with the current thetas
            new_cost = self.cost_function(theta0, theta1)

            print(str(alpha) + ' - ' + str(last_cost) + ' - ' + str(new_cost) + ' - ' + str(theta0) + ' - ' + str(theta1))

            # Decrease alpha if it is not converging
            if last_cost is None:
                tempTheta0 = theta0 - alpha * (1 / self.m) * sum(
                    (self.h(row.Grade, theta0, theta1) - row.Exam) for index, row in self.trainingSet.iterrows())
                tempTheta1 = theta1 - alpha * (1 / self.m) * sum(
                    (self.h(row.Grade, theta0, theta1) - row.Exam) * row.Grade for index, row in self.trainingSet.iterrows())
                future_cost = self.cost_function(tempTheta0, tempTheta1)
                if future_cost > new_cost:
                    alpha *= 0.99  # 0.33 was advised but make the gradient descent too long
                    continue

            # return thetas if the best values are found with an acceptable cost
            if last_cost is not None and i > 1000:
                return [theta0, theta1]

            # Calculate new thetas
            tempTheta0 = theta0 - alpha * (1 / self.m) * sum(
                (self.h(row.Grade, theta0, theta1) - row.Exam) for index, row in self.trainingSet.iterrows())
            tempTheta1 = theta1 - alpha * (1 / self.m) * sum(
                (self.h(row.Grade, theta0, theta1) - row.Exam) * row.Grade for index, row in
                self.trainingSet.iterrows())
            theta0 = tempTheta0
            theta1 = tempTheta1

            last_cost = new_cost
            i += 1

#Init

currentDir = os.path.dirname(os.path.realpath(__file__))

#Learning

classification = LogisticRegression_OneParam()
classification.set_training_set(pd.read_csv(os.path.join(currentDir, 'data', 'ExamData.csv'), parse_dates=False))
gd_result = classification.find_best_cost_function_with_gradient_descent()

#Plotting

classification.trainingSet.set_index('Grade')['Exam']
ax = classification.trainingSet[['Grade', 'Exam']].plot(x='Grade', style='o', label='Grades & Exam')

x = pylab.linspace(0,20,200)
plt.plot(x, classification.h(x, gd_result[0], gd_result[1]), 'r', label='Gradient Descent')

plt.show()