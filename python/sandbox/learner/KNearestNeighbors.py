import numpy as np
from sklearn import preprocessing, cross_validation
import sklearn
import datetime

from sklearn.metrics import accuracy_score

from python.sandbox.learner import Learner

class KNearestNeighbors(Learner):

    def __init__(self, k=5):
        Learner.__init__(self)
        self.k = k
        self.number_of_days = None
        self.knn = None
        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None

    def train(self, test_ratio):
        if self.data is not None:
            X = np.array(self.data.drop(['label'], 1))
            X = preprocessing.scale(X)
            X = X[:-self.number_of_days]
            Y = np.array(self.data['label'])[:-self.number_of_days]

            self.X_train, self.X_test, self.y_train, self.y_test = \
                cross_validation.train_test_split(X, Y, test_size=test_ratio)
        else:
            raise Exception("Data of the learner is None. Call 'read_data' first.")

    def get_accuracy(self):
        if self.X_train is not None:
            self.knn = sklearn.neighbors.KNeighborsClassifier(n_neighbors=self.k)
            self.knn.fit(self.X_train, self.y_train)
            accuracy = accuracy_score(self.y_test, self.knn.predict(self.X_test))
            return accuracy
        else:
            raise Exception("Training data of the learner is None. Call 'train' first.")

    def forecast(self):
        if self.knn is not None:

            X = np.array(self.data.drop(['label'], 1))
            X = preprocessing.scale(X)

            X_to_predict = X[-self.number_of_days:]
            forecast_set = self.knn.predict(X_to_predict)

            last_date = self.data.iloc[-1].name
            last_unix = last_date.timestamp()
            one_day = 86400
            next_unix = last_unix + one_day

            for i in forecast_set:
                next_date = datetime.datetime.fromtimestamp(next_unix)
                next_unix += 86400
                self.data.loc[next_date] = [np.nan for _ in range(len(self.data.columns) - 1)] + [i]
        else:
            raise Exception("LinearRegression object is None. Call 'get_accuracy' first.")


