import numpy as np
from sklearn import preprocessing, cross_validation
import sklearn
import datetime
from python.sandbox.learner import Learner

class SvmRegression(Learner):

    def __init__(self):
        Learner.__init__(self)
        self.clf = None
        self.number_of_days = None
        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None

    def train(self, test_ratio):
        if self.data is not None:
            X = np.array(self.data.drop(['label'], 1))
            X = preprocessing.scale(X)
            X_to_predict = X[-self.number_of_days:]
            X = X[:-self.number_of_days]
            Y = np.array(self.data['label'])[:-self.number_of_days]

            self.X_train, self.X_test, self.y_train, self.y_test = \
                cross_validation.train_test_split(X, Y, test_size=test_ratio)
        else:
            raise Exception("Data of the learner is None. Call 'read_data' first.")

    def get_accuracy(self):
        if self.X_train is not None:
            self.clf = sklearn.svm.SVR()
            self.clf.fit(self.X_train, self.y_train)
            linear_accuracy = self.clf.score(self.X_test, self.y_test)
            return linear_accuracy
        else:
            raise Exception("Training data of the learner is None. Call 'train' first.")

    def forecast(self):
        if self.clf is not None:
            X = np.array(self.data.drop(['label'], 1))
            X = preprocessing.scale(X)
            X_to_predict = X[-self.number_of_days:]
            forecast_set = self.clf.predict(X_to_predict)

            last_date = self.data.iloc[-1].name
            last_unix = last_date.timestamp()
            one_day = 86400
            next_unix = last_unix + one_day

            for i in forecast_set:
                next_date = datetime.datetime.fromtimestamp(next_unix)
                next_unix += 86400
                self.data.loc[next_date] = [np.nan for _ in range(len(self.data.columns) - 1)] + [i]
        else:
            raise Exception("LinearRegression object is None. Call 'get_accuracy' first.")