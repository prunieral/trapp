from python.tool.ColumnBuilder import ColumnBuilder as cb
from python.tool.DataReader import DataReader as dr

class Learner():

    def __init__(self):
        self.initial_data = None
        self.data = None
        self.dr = dr()

    def read_data(self, exchange, stock, index='Date', usecols=None):
        self.data = self.dr.read_specific_file(exchange, stock, index=index, usecols=usecols)
        self.initial_data = self.data

    def get_date_range(self, begin_date, end_date):
        self.data = self.data.ix[begin_date:end_date]

    def add_column(self, column_name):
        if column_name == "hl_pct":
            self.data = cb.build_hl_pct(self.data)
        if column_name == "pct_change":
            self.data = cb.build_pct_change(self.data)
        return None

    def add_future_column_as_label(self, number_of_days, col_name='Close'):
        if self.data is not None:
            self.data = cb.build_future_column(self.data,  col_name='label', time_in_future=number_of_days, ref_col=col_name)
            self.number_of_days = number_of_days
        else:
            raise Exception("Data of the learner is None. Call 'read_data' first.")

    def add_increase_column_as_label(self, number_of_days):
        if self.data is not None:
            self.data = cb.build_approximative_return(self.data, 'label', past_time=number_of_days)
            self.number_of_days = number_of_days
        else:
            raise Exception("Data of the learner is None. Call 'read_data' first.")