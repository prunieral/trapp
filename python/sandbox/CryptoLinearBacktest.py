import sys
import os
from python.tool.DataFisher import DataFisher
from python.tool.ColumnBuilder import ColumnBuilder
from python.object.Portfolio import Portfolio
from python.tool.DataReader import DataReader
from python.sandbox.learner import LinearRegression
import pandas as pd

sys.path.append('..')


class CryptoLinearBacktest:

    @staticmethod
    def test(date_start,
             columns={},
             performance_buy_trigger=1,
             min_accuracy=0.9,
             future_column_time=5,
             fee=0.001,
             verbose=False,
             download_data=False,
             save_result=False):

        # Parameters
        timestamp_start = pd.Timestamp(date_start)
        portfolio = Portfolio(30)
        learners = {}

        # Get the data
        if download_data:
            DataFisher.get_all_USDT_crypto('2018-07-01')

        # Get the filename of the data
        crypto_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../data/', 'CRYPTO')
        files = [file.split('.')[0] for file in os.listdir(crypto_path)]

        # Get all the dates
        reader = DataReader()
        random_df = reader.read_specific_file('CRYPTO', files[0], index='date')
        dates = random_df.index.tolist()

        # Start the test
        for date in [d for d in dates if d > timestamp_start]:
            print(f'NEW DATE : {date} - {portfolio.get_total_asset()}') if verbose else None
            for file in files:

                # Learning
                if not learners.keys().__contains__(file):
                    learner = LinearRegression()
                    learner.read_data('CRYPTO', file, index='date', usecols=['date', 'close'])
                    learner.data = ColumnBuilder.build(learner.data, columns)
                    learner.add_future_column_as_label(future_column_time, col_name='close')
                    learner.get_date_range('2018-01-01 00:00:00', date)
                    learner.data = learner.data.fillna(0)
                    learner.train(0.7)
                    learner.get_accuracy()

                    learners[file] = learner
                    continue
                else:
                    learner = learners[file]

                    if date in learner.initial_data.index:
                        set_to_forecast = learner.initial_data[date:date]
                        set_to_forecast = set_to_forecast.drop(['label'], 1)

                        current_price = learner.initial_data.loc[date, 'close']
                        forecast_price = learner.simple_forecast(set_to_forecast)
                        performance = (forecast_price * 100 / current_price) - 100
                    else:
                        continue

                # Sell if the results are bad
                if len(portfolio.has_value(file)) > 0 and performance < 0:
                    portfolio.close_position(portfolio.has_value(file)[0].id, date, current_price, fee)
                    print(f"SELL : {file} - {date} - {current_price}") if verbose else None

                # Buy if the results are good
                if performance > performance_buy_trigger and portfolio.current_asset > 1 and learner.get_accuracy() > min_accuracy:
                    buying_fee = portfolio.current_asset * fee
                    quantity = (portfolio.current_asset - (portfolio.current_asset * fee)) / current_price
                    portfolio.open_position(file, current_price, date, quantity, buying_fee)
                    print(f"BUY : {file} - {date} - {current_price} - {quantity}") if verbose else None

        # Write result in files
        if save_result:
            if not os.path.isdir(os.path.join('..', '..', 'data', 'REPORT')):
                os.mkdir(os.path.join('..', '..', 'data', 'REPORT'))
            if not os.path.isdir(os.path.join('..', '..', 'data', 'REPORT', 'crypto_linear_backtest')):
                os.mkdir(os.path.join('..', '..', 'data', 'REPORT', 'crypto_linear_backtest'))

            columns_name = '.'.join(columns.keys())
            file = open(os.path.join('..', '..', 'data', 'REPORT', 'crypto_linear_backtest',
                        f'{date_start}-{columns_name}-'
                        f'{performance_buy_trigger}-{min_accuracy}-'
                        f'{future_column_time}-{fee}.dsv'), 'w')
            file.write(portfolio.print_all_positions)
            file.close()

        print(f'FINAL ASSET :  {portfolio.get_total_asset()}') if verbose else None
        print(portfolio.print_all_positions) if verbose else None

        return portfolio, date_start
