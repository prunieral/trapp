import sys
import pandas as pd
from python.tool.DataReader import DataReader
from python.tool.ColumnBuilder import ColumnBuilder
from sklearn import preprocessing
from sklearn.model_selection import train_test_split, cross_validate, learning_curve
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.pipeline import Pipeline
import matplotlib.pyplot as plt
import numpy as np

sys.path.append('..')

# PREPARE DATAFRAME

dataReader = DataReader()
stocks = dataReader.get_stocks()
dfs = []

for exchange in stocks.keys():
    for stock in stocks[exchange]:
        df = dataReader.read_specific_file(exchange, stock)
        df = ColumnBuilder.build_standard_deviation(df, col_name='STD_5', past_time=5)
        df = ColumnBuilder.build_standard_deviation(df, col_name='STD_20', past_time=20)
        df = ColumnBuilder.build_ewma(df, col_name='EWMA_5', past_time=5)
        df = ColumnBuilder.build_ewma(df, col_name='EWMA_20', past_time=20)
        df = ColumnBuilder.build_moving_average(df)
        df = ColumnBuilder.build_macd(df)
        df = ColumnBuilder.build_stochastic_oscillator(df)
        df = ColumnBuilder.build_commodity_channel_index(df)
        df = ColumnBuilder.build_relative_strength_index(df)
        df = ColumnBuilder.build_bollinger_bands(df)
        df = ColumnBuilder.build_average_true_range(df)
        df = ColumnBuilder.build_chaikin_oscillator(df)
        df = ColumnBuilder.build_on_balance_volume(df)
        df = ColumnBuilder.build_volume_rate_of_change(df)
        df = ColumnBuilder.build_future_bin_class_column(df, col_name='LABEL', time_in_future=10, percent=1)
        df = df.drop(columns=['Open', 'High', 'Low', 'Volume', 'Turnover'])
        dfs.append(df)

data = pd.concat(dfs)
data = data.dropna()
data['LABEL'] = data['LABEL'].astype(int)

# PRE-PROCESSING

X = data.iloc[:, :-1]
y = data.iloc[:, -1]
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=1, test_size=0)
X = preprocessing.scale(X)


# PREPARE AND VALUATE PIPELINES

def testtt(pipeline, X, y):
    train_sizes, train_scores, test_scores = learning_curve(estimator=pipeline,
                                                             X=X,
                                                             y=y,
                                                             train_sizes=np.linspace(0.1, 1.0, 20),
                                                             cv=10,
                                                             n_jobs=1)
    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)
    plt.plot(train_sizes, train_mean, color='blue', marker='o', markersize=5, label='training accuracy')
    plt.fill_between(train_sizes,
                     train_mean + train_std,
                     train_mean - train_std,
                     alpha=0.15, color='blue')
    plt.plot(train_sizes, test_mean,
             color='green', linestyle='--',
             marker='s', markersize=5,
             label='validation accuracy')
    plt.fill_between(train_sizes,
                     test_mean + test_std,
                     test_mean - test_std,
                     alpha=0.15, color='green')
    plt.grid()
    plt.xlabel('Number of training samples')
    plt.ylabel('Accuracy')
    plt.legend(loc='lower right')
    plt.ylim([0.8, 1.0])
    plt.show()


SVC_kernel = ['linear', 'poly', 'rbf', 'sigmoid']

for kernel in SVC_kernel:
    pipeline = Pipeline(steps=[('AA', SVC(kernel=kernel))])
    scores = cross_validate(pipeline, X, y, cv=5, return_train_score=True)
    print('SVC' + ' ' + kernel)
    print(str(scores['test_score'].mean()) + '\t' +
          str(scores['test_score'].std()) + '\t' +
          str(scores['train_score'].mean()) + '\t' +
          str(scores['train_score'].std()) + '\t')
    testtt(pipeline, X, y)

for n in range(2,10):
    pipeline = Pipeline(steps=[('AA', KNeighborsClassifier(n_neighbors=n))])
    scores = cross_validate(pipeline, X, y, cv=5, return_train_score=True)
    print('KNN' + ' ' + str(n))
    print(str(scores['test_score'].mean()) + '\t' +
          str(scores['test_score'].std()) + '\t' +
          str(scores['train_score'].mean()) + '\t' +
          str(scores['train_score'].std()) + '\t')
    testtt(pipeline, X, y)

pipeline = Pipeline(steps=[('AA', DecisionTreeClassifier())])
scores = cross_validate(pipeline, X, y, cv=5, return_train_score=True)
print('DTC' + ' ')
print(str(scores['test_score'].mean()) + '\t' +
      str(scores['test_score'].std()) + '\t' +
      str(scores['train_score'].mean()) + '\t' +
      str(scores['train_score'].std()) + '\t')
testtt(pipeline, X, y)

pipeline = Pipeline(steps=[('AA', RandomForestClassifier())])
scores = cross_validate(pipeline, X, y, cv=5, return_train_score=True)
print('RFC' + ' ')
print(str(scores['test_score'].mean()) + '\t' +
      str(scores['test_score'].std()) + '\t' +
      str(scores['train_score'].mean()) + '\t' +
      str(scores['train_score'].std()) + '\t')
testtt(pipeline, X, y)

# DIAGNOSES BIAS AND VARIANCE WITH LEARNING CURVES




