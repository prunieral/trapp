import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))
from python.ml.ModelAssessor import ModelAssessor
from python.mail.Mail import Mail

if __name__ == '__main__':
    try:
        ignored_cols = ['Open', 'High', 'Low', 'Volume', 'Turnover', 'Stock']
        ma = ModelAssessor()
        ma.process(None, ignored_cols)
    except Exception as e:
        Mail.send_error_mail(str(e))