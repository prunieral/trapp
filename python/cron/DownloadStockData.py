import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))
from python.tool.DataFisher import DataFisher
from python.mail.Mail import Mail

try:
    DataFisher.get_all_stocks()
except Exception as e:
    Mail.send_error_mail(str(e))