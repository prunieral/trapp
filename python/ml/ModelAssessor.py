import sys
import pandas as pd
from datetime import datetime
from python.tool.DataReader import DataReader
from python.tool.ColumnBuilder import ColumnBuilder
from python.db.DB import DB
from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.pipeline import Pipeline
import numpy as np

sys.path.append('..')


class ModelAssessor:

    def __init__(self):
        self.db = DB()

    def process(self, end_date=None, ignored_columns=[]):

        # INIT VARIABLES

        db = DB()
        assessment_name = '_'.join(['MA', datetime.now().strftime('%y%m%d_%H%M%S')])

        # PREPARE DATAFRAME

        data = self.get_input_data(self.get_config(), end_date)
        data = data.drop(ignored_columns, axis=1)

        # STANDARDIZATION & PRE-PROCESSING

        x = data.iloc[:, :-1]
        y = data.iloc[:, -1].astype(int)

        for col, v in np.isfinite(x).all().iteritems():
            if not v:
                print('ERROR - This column contains infinite value(s) : ' + str(col))
                
        x = preprocessing.scale(x)

        for col in x:
            if col.dtype != float:
                print('WARNING - Column not in float type : ' + str(col))


        # PREPARE AND EVALUATE PIPELINES

        pipelines = {'knn': {'pipeline': Pipeline([('knn', KNeighborsClassifier())])},
                     'dtc': {'pipeline': Pipeline([('dtc', DecisionTreeClassifier())])},
                     'rfc': {'pipeline': Pipeline([('rfc', RandomForestClassifier())])},
                     'gbc': {'pipeline': Pipeline([('gbc', GradientBoostingClassifier())])},
                     'svc': {'pipeline': Pipeline([('svc', SVC(probability=True))])}}

        pipelines['knn']['parameter'] = [{
            'knn__n_neighbors': range(2, 7, 1)
            }]

        pipelines['dtc']['parameter'] = [{
            'dtc__min_samples_split': range(2, 50, 10),
            'dtc__criterion': ['gini', 'entropy']
            }]

        pipelines['rfc']['parameter'] = [{
            'rfc__criterion': ['gini', 'entropy'],
            'rfc__n_estimators': range(10, 1000, 200),
            'rfc__max_depth': range(3, 20, 4),
            'rfc__min_samples_leaf': range(50, 1000, 200)
            }]

        pipelines['gbc']['parameter'] = [{
            'gbc__n_estimators': range(10, 1000, 200),
            'gbc__max_depth': range(3, 20, 4),
            'gbc__min_samples_leaf': range(50, 1000, 200),
            'gbc__learning_rate': np.arange(.005, .1, .05)
            }]

        pipelines['svc']['parameter'] = [{
            'svc__kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
            }]

        db.insert_assessment_head(assessment_name, end_date)

        for key, p in pipelines.items():
            self.__process_assessment(assessment_name, p['pipeline'], p['parameter'], x, y, key)

        return assessment_name

    def get_input_data(self, end_date=None):
        data_reader = DataReader()
        stocks = data_reader.get_stocks()
        dfs = []

        for exchange in stocks.keys():
            for stock in stocks[exchange]:
                df = data_reader.read_specific_file(exchange, stock)
                if end_date is not None:
                    df = df[(df.index < end_date)]
                df = ColumnBuilder.build(df, self.get_config())
                dfs.append(df)

        data = pd.concat(dfs)
        data = data.dropna()

        return data

    def __process_assessment(self, assessment_name, pipeline, params, x, y, algorithm):
        gs = GridSearchCV(estimator=pipeline,
                          param_grid=params,
                          scoring=['accuracy', 'precision', 'f1', 'recall', 'neg_log_loss', 'roc_auc'],
                          refit='neg_log_loss',
                          cv=5,
                          n_jobs=-1,
                          return_train_score=True)
        gs.fit(x, y)
        rows_to_insert = self.organize_grid_result(gs, algorithm)
        self.db.insert_assessment_details(assessment_name, rows_to_insert)

    @staticmethod
    def organize_grid_result(gs, algorithm):
        rows_to_insert = []
        for i in range(0, len(gs.cv_results_[list(gs.cv_results_.keys())[0]]) - 1):
            row = {}
            for key in gs.cv_results_.keys():
                row[key] = int(gs.cv_results_[key][i]) if type(gs.cv_results_[key][i]) is np.int32 \
                    else gs.cv_results_[key][i]
            row['algorithm'] = algorithm
            rows_to_insert.append(row)
        return rows_to_insert

    @staticmethod
    def get_config():
        return([
            ('build_macd', {'ema_short': 12, 'ema_long': 26, 'normalized': True}),
            ('build_macd', {'ema_short': 6, 'ema_long': 12, 'normalized': True}),
            ('build_stochastic_oscillator', {'past': 5}),
            ('build_stochastic_oscillator', {'past': 10}),
            ('build_commodity_channel_index', {'past': 5}),
            ('build_commodity_channel_index', {'past': 10}),
            ('build_relative_strength_index', {'past': 5}),
            ('build_relative_strength_index', {'past': 10}),
            # ('build_percent_b', {'past': 5}), # BUG
            ('build_percent_b', {'past': 10}),
            ('build_average_true_range', {'past': 5, 'normalized': True}),
            ('build_average_true_range', {'past': 10, 'normalized': True}),
            ('build_standard_deviation', {'past': 5}),
            ('build_standard_deviation', {'past': 10}),
            ('build_chaikin_oscillator', {}),
            ('build_normalized_volume', {'past': 5}),
            ('build_normalized_volume', {'past': 20}),
            ('build_future_bin_class_column', {'col_name': 'LABEL', 'future': 10, 'percent': .2}),
        ])


if __name__ == '__main__':
    ignored_cols = ['Open', 'High', 'Low', 'Volume', 'Turnover', 'Stock']
    ma = ModelAssessor()
    ma.process(None, ignored_cols)

