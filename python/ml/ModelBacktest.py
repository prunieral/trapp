import sys
from python.ml.ModelAssessor import ModelAssessor
from python.db.DB import DB
from python.object.Portfolio import Portfolio
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.pipeline import Pipeline
from sklearn import preprocessing
from datetime import datetime
import pandas as pd
import numpy as np

sys.path.append('..')


class ModelBacktest:

    def __init__(self):
        pd.options.mode.chained_assignment = None
        self.db = DB()

    def process(self, capital, start_date, algorithm, params={}, ignored_columns=[]):

        # INIT VARIABLES

        ma = ModelAssessor()
        return_data = {}
        pipeline = None

        # GET THE DATA

        data = ma.get_input_data()
        x = data.iloc[:, :-1]
        y = data.iloc[:, -1].astype(int)
        x_full = x[x.index >= start_date]
        x = x[x.index < start_date]
        x = x[[e for e in x.columns if e not in ignored_columns]]
        y = y[y.index < start_date]
        x = preprocessing.scale(x)

        # BUILD THE MODEL

        for k in list(params):
            v = params.pop(k)
            params[k.split('__')[1]] = v

        if algorithm == 'knn':
            pipeline = Pipeline([('knn', KNeighborsClassifier(**params))])
        elif algorithm == 'dtc':
            pipeline = Pipeline([('dtc', DecisionTreeClassifier(**params))])
        elif algorithm == 'rfc':
            pipeline = Pipeline([('rfc', RandomForestClassifier(**params))])
        elif algorithm == 'gbc':
            pipeline = Pipeline([('gbc', GradientBoostingClassifier(**params))])
        elif algorithm == 'svc':
            pipeline = Pipeline([('svc', SVC(**params))])
        else:
            print('ERROR - Algorithm not found : ' + algorithm)

        pipeline.fit(x, y)

        # PROCESS

        pf = Portfolio(capital)

        dates = list(set(x_full.index))
        dates.sort()

        for i, date in enumerate(dates):
            x_date = x_full[x_full.index == date]
            x_filtered = x_date[[e for e in x_date.columns if e not in ignored_columns]]
            predictions = pipeline.predict(x_filtered)
            x_date['LABEL'] = predictions

            for pos in pf.get_opened_positions():
                if len([d for d in dates if date > d > pos.start_date]) >= 5 and \
                        len(x_date.loc[x_date['Stock'] == pos.value]) == 1 and \
                        x_date.loc[x_date['Stock'] == pos.value].iloc[0]['LABEL'] == 0:
                    price = x_full.loc[(x_full['Stock'] == pos.value) & (x_full.index == dates[i + 1])].iloc[0]['Open']
                    pf.close_position(pos.id, dates[i + 1], price, 1)

            for index, stock in x_date.iterrows():
                if stock['LABEL'] == 1 and pf.current_asset > (pf.start_asset / 10) and \
                        not pf.has_value(stock['Stock']):
                    asset = pf.start_asset / 5 if pf.current_asset > (pf.start_asset / 5) else pf.current_asset
                    price = x_full.loc[(x_full['Stock'] == stock['Stock']) & (x_full.index == dates[i + 1])].Open
                    quantity = int(asset / price)
                    pf.open_position(stock['Stock'], price, dates[i + 1], quantity, 1)

        # PREPARE OUTPUT

        return_data["capital"] = pf.current_asset
        return_data["result"] = pf.start_asset
        return_data["positions"] = pf.positions
        return_data["performances"] = self.get_performance(x_full, pf).to_dict('records')

        print(return_data)
        return return_data

    @staticmethod
    def get_performance(data, portfolio):
        return_data = pd.DataFrame(index=data.index)
        return_data['asset'] = np.nan
        cash_asset = portfolio.start_asset
        for d in data.index:
            stock_asset = 0
            for p in portfolio.positions:
                if d == p.start_date:
                    cash_asset -= (p.start_price * p.quantity) + p.fee
                if d == p.end_date:
                    cash_asset += (p.end_price * p.quantity) + p.fee
                if p.start_date <= d < p.start_date:
                    stock_asset += p.quantity * data.loc[(data.index == d) & (data['Stock'] == p.value)]['Close'].iloc[0]
            return_data['asset'][d] = cash_asset + stock_asset

        return_data = return_data.reset_index()

        return return_data


if __name__ == '__main__':
    date = datetime.strptime('2016-01-01', '%Y-%m-%d')
    ignored_cols = ['Open', 'High', 'Low', 'Volume', 'Turnover', 'Stock']
    mb = ModelBacktest()
    mb.process(10000, date, 'knn', {}, ignored_cols)










