import pandas as pd
import os


class DataReader:

    data_path = None

    def __init__(self):
        self.data_path = os.path.join(os.path.dirname(__file__), '../../data/')

    def get_exchanges(self):
        return [file for file in os.listdir(self.data_path) if os.path.isdir(self.data_path + file)]

    def get_stocks(self):
        stocks = {}
        for file in os.listdir(self.data_path):
            if os.path.isdir(self.data_path + file):
                stocks[file] = [csv.split('.')[0] for csv in os.listdir(self.data_path + file)]
        return stocks

    def read_specific_file(self, exchange, stock, index='Date', usecols=None):
        df = pd.read_csv(os.path.join(self.data_path, exchange, stock + ".csv"),
                    index_col=index,
                    usecols=usecols,
                    parse_dates=True,
                    na_values=['NaN'])
        df['Stock'] = exchange + '/' + stock
        return df

