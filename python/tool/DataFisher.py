import os
import shutil
import sys
import requests
from pandas_datareader._utils import RemoteDataError
import pandas as pd
import quandl
import time
import datetime

sys.path.append('..')


class DataFisher:

    @staticmethod
    def get_all_stocks():

        data_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../data/')

        for file in os.listdir(data_path):
            if os.path.isdir(data_path + file):
                shutil.rmtree(data_path + file)

        for file in [f for f in os.listdir(data_path) if f.__contains__('.txt')]:
            if os.path.isfile(data_path + file):
                os.mkdir(data_path + file.split('.')[0])

                with open(data_path + file) as f:
                    stock_names = f.readlines()

                    for stock_name in stock_names:
                        try:
                            stock_name = stock_name.replace('\n', '')
                            quandl.ApiConfig.api_key = "xJjuA5sJBPJRAQpGEV9g"
                            data = quandl.get(file.split('.')[0] + '/' + stock_name, start_date="2010-01-01")
                            data = data.rename(columns={'Last': 'Close'})
                            data.to_csv(os.path.join(data_path, file.split('.')[0], stock_name + '.csv'))
                            print('Downloaded : ' + stock_name)
                        except RemoteDataError as e:
                            print(e)

    @staticmethod
    def get_all_USDT_crypto(start_date):

        data_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../data/')
        unix_time = time.mktime(datetime.datetime.strptime(start_date, "%Y-%m-%d").timetuple())

        r = requests.get('https://poloniex.com/public?command=returnTicker').json()

        if os.path.isdir(os.path.join(data_path, 'CRYPTO')):
            shutil.rmtree(os.path.join(data_path, 'CRYPTO'))
        if not os.path.isdir(os.path.join(data_path, 'CRYPTO')):
            os.mkdir(os.path.join(data_path, 'CRYPTO'))

        for ticker in r:
            if 'USDT' in ticker:
                chart_data = requests.get('https://poloniex.com/public?command=returnChartData' +
                                 '&currencyPair=' + ticker +
                                 '&period=300' +
                                 '&start=' + str(unix_time) +
                                 '&end=9999999999').json()
                chart_data_df = pd.DataFrame(chart_data)
                chart_data_df['date'] = pd.to_datetime(chart_data_df['date'], unit='s')
                chart_data_df = chart_data_df.set_index('date')
                chart_data_df.to_csv(os.path.join(data_path, 'CRYPTO', ticker + '.csv'))

        return None