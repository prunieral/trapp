import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as m3d

class PlotDrawer:

    @staticmethod
    def plot_dataframe(df):
        df[list(df)].plot()
        plt.show()

    @staticmethod
    def plot_dataframe_with_bollinger(df):

        rm = pd.rolling_mean(df, window=20)
        rstd = pd.rolling_std(df, window=20)

        upper_band = rm + (rstd * 2)
        lower_band = rm - (rstd * 2)

        ax = df['SPY'].plot(title="Bollinger Bands", label='SPY')
        rm.plot(label='Rolling mean', ax=ax)
        upper_band.plot(label='upper band', ax=ax)
        lower_band.plot(label='lower band', ax=ax)

        plt.show()

    @staticmethod
    def plot_line(slope, intercept):
        N = 2
        points = np.linspace(-10, 10, N)
        plt.plot(points, slope * points + intercept)

        plt.show()

    @staticmethod
    def plot_3d_linear_regression(data):
        # Plot all the 3D points in the Data
        # Calculate de Linear Regression in the 3D Environment
        # Plot the calculated Line
        datamean = data.mean(axis=0)
        uu, dd, vv = np.linalg.svd(data - datamean)
        linepts = vv[0] * np.mgrid[-100:100:2j][:, np.newaxis]
        linepts += datamean.values

        ax = m3d.Axes3D(plt.figure())
        for x, y, z in data.values.tolist():
                ax.scatter(x, y, z)
        ax.plot3D(*linepts.T)
        plt.show()