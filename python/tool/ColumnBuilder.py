import numpy as np
import pandas as pd


class ColumnBuilder:

    @staticmethod
    def build(df, config):
        for column, params in config:
            func = getattr(ColumnBuilder, column)
            df = func(df, **params)
        return df

    @staticmethod
    def build_future_column(df, col_name=None, future=5, ref_col='Close'):
        col_name = col_name if col_name is not None else '_'.join(['LABL_FUTURE', str(future)])
        df[col_name] = df[ref_col].shift(-future)
        return df

    @staticmethod
    def build_future_bin_class_column(df, col_name=None, future=5, percent=1, ref_col='Close'):
        col_name = col_name if col_name is not None else '_'.join(['LABL_FUTURE_BIN_CLASS', str(future), str(percent)])
        df[col_name] = np.where((df[ref_col].shift(-future) - df[ref_col]) / df[ref_col] > percent, 1, 0)
        return df

    @staticmethod
    def build_future_category_column(df, col_name=None, future=5, ref_col='Close'):
        col_name = col_name if col_name is not None else '_'.join(['LABL_FUTURE_CATEGORY', str(future)])
        df[col_name] = np.log(df[ref_col].shift(-future) / df[ref_col])
        conditions = [df[col_name] > 0.015, df[col_name] > 0.005, df[col_name] < -0.015, df[col_name] > -0.005]
        choices = [2, 1, -1, -2]
        df[col_name] = np.select(conditions, choices, default=0)
        return df

    @staticmethod
    def build_standard_deviation(df, col_name=None, past=5, ref_col='Close', normalized=False):
        col_name = col_name if col_name is not None else '_'.join(['CALC_STD', str(past)])
        df[col_name] = df[ref_col].rolling(window=past, center=False).std()
        if normalized:
            df[col_name] = df[col_name] / df[ref_col].rolling(window=past, center=False).mean()
        return df

    @staticmethod
    def build_ewma(df, col_name=None, past=5, ref_col='Close'):
        col_name = col_name if col_name is not None else '_'.join(['CALC_EWMA', str(past)])
        df[col_name] = df[ref_col].ewm(span=past).mean()
        return df

    @staticmethod
    def build_moving_average(df, col_name=None, past=5, ref_col='Close'):
        col_name = col_name if col_name is not None else '_'.join(['CALC_MOV_AVERAGE', str(past)])
        df[col_name] = df[ref_col].rolling(window=past, center=False).mean()
        return df

    @staticmethod
    def build_macd(df, col_name=None, ema_short=12, ema_long=26, ref_col='Close', normalized=False):
        col_name = col_name if col_name is not None else '_'.join(['CALC_MACD', str(ema_short), str(ema_long)])
        df_short = df[ref_col].ewm(span=ema_short).mean()
        df_long = df[ref_col].ewm(span=ema_long).mean()
        df[col_name] = df_short - df_long
        if normalized:
            df[col_name] = df[col_name] / df_long
        return df

    @staticmethod
    def build_stochastic_oscillator(df, col_name=None, past=5):
        col_name = col_name if col_name is not None else '_'.join(['CALC_STO_OSCI', str(past)])
        sok = pd.Series((df['Close'] - df['Low'].rolling(past).min()) /
                        (df['High'].rolling(past).max() - df['Low'].rolling(past).min()), name=col_name)
        sok = sok.rolling(window=past, center=False).mean()
        df = df.join(sok)
        return df

    @staticmethod
    def build_stochastic_oscillator_d(df, col_name=None, past=5):
        col_name = col_name if col_name is not None else '_'.join(['CALC_STO_OSCI_D_', str(past)])
        sok = pd.Series((df['Close'] - df['Low'].rolling(past).min()) /
                        (df['High'].rolling(past).max() - df['Low'].rolling(past).min()), name=col_name + '_K')
        sod = pd.Series(sok.rolling(window=past, center=False).mean(), name=col_name)
        sod = sod.rolling(window=past, center=False).mean()
        df = df.join(sod)
        return df

    @staticmethod
    def build_commodity_channel_index(df, col_name=None, past=5):
        col_name = col_name if col_name is not None else '_'.join(['CALC_CCI', str(past)])
        pp = (df['High'] + df['Low'] + df['Close']) / 3
        cci = pd.Series((pp - pp.rolling(past).mean()) / pp.rolling(past).std(), name=col_name)
        df = df.join(cci)
        return df

    @staticmethod
    def build_relative_strength_index(df, col_name=None, past=5):
        col_name = col_name if col_name is not None else '_'.join(['CALC_RSI', str(past)])
        i = 0

        upi = pd.DataFrame(index=df.index)
        doi = pd.DataFrame(index=df.index)
        upi['value'] = np.nan
        doi['value'] = np.nan

        while i < len(df.index) - 1:
            up_move = df.ix[i+1]['High'] - df.ix[i]['High']
            do_move = df.ix[i]['Low'] - df.ix[i+1]['Low']
            if up_move > do_move and up_move > 0:
                upd = up_move
            else:
                upd = 0
            upi.ix[i]['value'] = upd
            if do_move > up_move and do_move > 0:
                dod = do_move
            else:
                dod = 0
            doi.ix[i]['value'] = dod
            i = i + 1
        pos_di = pd.Series(upi['value'].ewm(span=past, min_periods=past - 1).mean())
        neg_di = pd.Series(doi['value'].ewm(span=past, min_periods=past - 1).mean())
        rsi = pd.Series(pos_di / (pos_di + neg_di), name=col_name)
        df = df.join(rsi)
        return df

    @staticmethod
    def build_bollinger_bands(df, col_name=None, past=5):
        col_name = col_name if col_name is not None else '_'.join(['CALC_BOL_BANDS', str(past)])
        ma = pd.Series(df['Close'].rolling(past).mean())
        msd = pd.Series(df['Close'].rolling(past).std())
        b1 = ma + (msd * 2)
        band1 = pd.Series(b1, name=col_name + '_UP' + str(past))
        df = df.join(band1)
        b2 = ma - (msd * 2)
        band2 = pd.Series(b2, name=col_name + '_DOWN' + str(past))
        df = df.join(band2)
        return df

    @staticmethod
    def build_percent_b(df, col_name=None, past=5):
        col_name = col_name if col_name is not None else '_'.join(['PERCENT_B', str(past)])
        ma = pd.Series(df['Close'].rolling(past).mean())
        msd = pd.Series(df['Close'].rolling(past).std())
        b1 = ma + (msd * 2)
        upper_band = pd.Series(b1, name='tmp_CALC_BB_UP' + str(past))
        b2 = ma - (msd * 2)
        lower_band = pd.Series(b2, name='tmp_CALC_BB_DOWN' + str(past))
        df[col_name] = (df['Close'] - lower_band) / (upper_band - lower_band)
        return df

    @staticmethod
    def build_average_true_range(df, col_name=None, past=5, normalized=False):
        col_name = col_name if col_name is not None else '_'.join(['CALC_ATR', str(past)])
        i = 0

        tr_l = pd.DataFrame(index=df.index)
        tr_l['value'] = np.nan

        while i < len(df.index) - 1:
            tr = max(df.ix[i+1]['High'], df.ix[i]['Close']) - min(df.ix[i+1]['Low'], df.ix[i]['Close'])
            tr_l.ix[i]['value'] = tr
            i = i + 1
        atr = pd.Series(tr_l['value'].ewm(span=past, min_periods=past).mean(), name=col_name)
        df = df.join(atr)
        if normalized:
            df[col_name] = df[col_name] / df['Close'] * 100
        return df

    @staticmethod
    def build_chaikin_oscillator(df, col_name=None, normalized=False):
        col_name = col_name if col_name is not None else '_'.join(['CALC_CHAIKIN'])
        ad = (2 * df['Close'] - df['High'] - df['Low']) / (df['High'] - df['Low']) * df['Volume']
        chaikin = pd.Series(ad.ewm(span=3, min_periods=2).mean() - ad.ewm(span=10, min_periods=9).mean(), name=col_name)
        df = df.join(chaikin)
        if normalized:
            df[col_name] = df[col_name] / max(df[col_name].abs)
        return df

    @staticmethod
    def build_on_balance_volume(df, col_name=None):
        col_name = col_name if col_name is not None else '_'.join(['CALC_OBV'])
        i = 0

        obv = pd.DataFrame(index=df.index)
        obv['value'] = np.nan

        while i <= len(df.index) - 1:
            if i == 0:
                obv.ix[i]['value'] = 0
            else:
                if df.ix[i]['Close'] - df.ix[i - 1]['Close'] > 0:
                    obv.ix[i]['value'] = obv.ix[i - 1]['value'] + df.ix[i]['Volume']
                if df.ix[i]['Close'] - df.ix[i - 1]['Close'] == 0:
                    obv.ix[i]['value'] = obv.ix[i - 1]['value']
                if df.ix[i]['Close'] - df.ix[i - 1]['Close'] < 0:
                    obv.ix[i]['value'] = obv.ix[i - 1]['value'] - df.ix[i]['Volume']
            i = i + 1
        obv_ma = pd.Series(obv['value'], name=col_name)
        df = df.join(obv_ma)
        return df

    @staticmethod
    def build_volume_rate_of_change(df, col_name=None, past=5):
        col_name = col_name if col_name is not None else '_'.join(['CALC_VROC', str(past)])
        m = df['Volume'].diff(past - 1)
        n = df['Volume'].shift(past - 1)
        roc = pd.Series(m / n, name=col_name)
        df = df.join(roc)
        return df

    @staticmethod
    def build_normalized_volume(df, col_name=None, past=20):
        col_name = col_name if col_name is not None else '_'.join(['CALC_NORMALIZED_VOLUME', str(past)])
        m = df['Volume'].diff(past - 1)
        n = df['Volume'].rolling(window=past, center=False).mean()
        roc = pd.Series(m / n, name=col_name)
        df = df.join(roc)
        return df

