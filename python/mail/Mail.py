import subprocess
import smtplib


class Mail:

    @staticmethod
    def __send_mail(recipient, subject, body):

        mx_records = []
        mx_values = {'pref': 0, 'serv': ''}
        originator = 'automail@aprunier.com'
        domain = recipient.split("@")[1]

        p = subprocess.Popen('nslookup -type=mx ' + domain + ' 8.8.8.8', shell=True,
                             stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        for line in p.stdout.readlines():
            line = line.decode().lower()
            if line.find("mail exchanger") != -1:
                for char in line:
                    if str(char) in "\r\n\t":
                        line = line.replace(char, '')
                if line.find("mx preference") != -1:
                    mx_parse = line.replace(' ', '').split(",")
                    mx_values['pref'] = int(mx_parse[0].split("=")[1])
                    mx_values['serv'] = mx_parse[1].split("=")[1]
                else:
                    mx_parse = line.split(" = ")[1].split(" ")
                    mx_values['pref'] = int(mx_parse[0])
                    mx_values['serv'] = mx_parse[1]
                mx_records.append(mx_values.copy())

        retval = p.wait()

        def mx_pref_sortvalue(record):
            return record['pref']
        mx_records = sorted(mx_records, key=mx_pref_sortvalue)

        server = mx_records[0]['serv']

        smtp_send = smtplib.SMTP(server, 25)
        smtp_send.sendmail(originator, recipient, "From: " + originator + "\nTo: " + recipient +
                           "\nSubject:" + subject + "\n\n" + body)
        smtp_send.quit()

    @staticmethod
    def send_error_mail(body):
        Mail.__send_mail('prunier.al@gmail.com', 'ERROR', body)