from python.object.Position import Position


class Portfolio:

    start_asset = None
    current_asset = None
    positions = None

    def __init__(self, asset):
        self.start_asset = asset
        self.current_asset = asset
        self.positions = []

    def open_position(self, value, start_price, start_date, quantity, buying_fee=0):
        id = 0
        for position in self.positions:
            if position.id > id:
                id = position.id
        self.positions.append(Position(id+1, value, start_price, start_date, quantity, buying_fee))
        self.current_asset -= float((start_price * abs(quantity)) + buying_fee)

    def close_position(self, position, end_date, end_price, fee_in_percent=0):
        for idx, pos in enumerate(self.positions):
            if position == pos.id and pos.end_date is None:
                gain = end_price * abs(pos.quantity)
                fee = gain * fee_in_percent
                gain = gain - fee
                self.positions[idx].end_date = end_date
                self.positions[idx].end_price = end_price
                self.current_asset += gain
                self.positions[idx].selling_fee = fee

    def get_total_asset(self):
        asset = self.current_asset
        current_positions = [p for p in self.positions if p.end_date is None]
        for cp in current_positions:
            asset += cp.start_price * abs(cp.quantity)
        return asset

    def get_opened_positions(self):
        return [p for p in self.positions if p.end_date is None]

    def has_value(self, value):
        return True if len([p for p in self.positions if p.value == value and p.end_date is None]) > 0 else False

    @property
    def print_all_positions(self):
        ret = f'id\tvalue\tstart_price\tend_price\tquantity\tfee\n'
        for pos in [p for p in self.positions]:
            ret += f'{pos.id}\t' + \
                   f'{pos.value}\t' + \
                   f'{pos.start_date}\t' + \
                   f'{pos.start_price}\t' + \
                   f'{pos.end_date}\t' + \
                   f'{pos.end_price}\t' + \
                   f'{pos.quantity}\t' + \
                   f'{pos.fee}\n'
            return ret

    def print_current_positions(self):
        ret = f'id\tvalue\tstart_price\tend_price\tquantity\tfee\n'
        for pos in [p for p in self.positions if p.end_date is None]:
            ret += f'{pos.id}\t' + \
                   f'{pos.value}\t' + \
                   f'{pos.start_date}\t' + \
                   f'{pos.start_price}\t' + \
                   f'{pos.end_date}\t' + \
                   f'{pos.end_price}\t' + \
                   f'{pos.quantity}\t' + \
                   f'{pos.fee}\n'
        return ret
