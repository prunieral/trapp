

class Position:

    id = None
    value = None
    start_date = None
    end_date = None
    start_price = None
    end_price = None
    quantity = None
    buying_fee = None
    selling_fee = None

    def __init__(self, id, value, start_price, start_date, quantity, buying_fee=0):
        self.id = id
        self.value = value
        self.start_date = start_date
        self.start_price = start_price
        self.quantity = quantity
        self.buying_fee = buying_fee
        self.selling_fee = 0

    def __str__(self):
        return {'id': self.id,
                'value': self.value,
                'start_date': self.start_date,
                'start_price': self.start_price,
                'quantity': self.quantity,
                'buying_fee': self.buying_fee,
                'selling_fee': self.selling_fee}