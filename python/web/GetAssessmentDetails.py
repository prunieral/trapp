import sys
import os
import json
from python.db.DB import DB

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))

db = DB()

if len(sys.argv) == 2:
    result = db.get_assessment_details(sys.argv.pop(1))
    for i, row in enumerate(result):
        row['id'] = i + 1
        for key in [k for k in row.keys() if 'split' in k or 'param_' in k or k in ['date','name','type']]:
            del row[key]

    print(json.dumps(result).replace(' ', ''))
else:
    print('error')

