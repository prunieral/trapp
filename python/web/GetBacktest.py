import sys
import os
import json

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..','..'))

sys.argv.pop(0)

if len(sys.argv) == 3:
    print(json.dumps("").replace(' ', ''))
else:
    print({'error': 'The length of the params is incorrect'})
    sys.exit()
