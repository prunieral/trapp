import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..','..'))
from timezonefinder import TimezoneFinder
import pytz
import datetime
import json

if len(sys.argv) != 1:
    print({'error': 'The length of the params is incorrect. Should be 0'})
    sys.exit()

def get_hour(value):
    return int(value[0:2])

def get_minute(value):
    return int(value[3:5])

def get_time_left(time, hour, minute, add_day=0):
    targeted_time = time + datetime.timedelta(days=add_day)
    targeted_time = targeted_time.replace(hour=hour, minute=minute)
    return (targeted_time-time).total_seconds()

exchanges = [
  { 'name': "NYSE", 'coordinates': [-74.0060,40.7128], 'open': '09:00', 'close': '16:00',
    'lunch_close': False, 'lunch_open': False},
  { 'name': "NASDAQ", 'coordinates': [-74.0060,37.7128], 'open': '09:00', 'close': '16:00',
    'lunch_close': False, 'lunch_open': False},
  { 'name': "Euronext", 'coordinates': [2.3522,48.8566], 'open': '09:00', 'close': '17:30',
    'lunch_close': False, 'lunch_open': False},
  { 'name': "Japan", 'coordinates': [139.6917,35.6895], 'open': '09:00', 'close': '15:00',
    'lunch_close': '11:30', 'lunch_open': '12:30'},
  { 'name': "London", 'coordinates': [-1.5491,53.8008], 'open': '08:00', 'close': '16:30',
    'lunch_close': False, 'lunch_open': False},
  { 'name': "Swiss", 'coordinates': [6.1432,46.2044], 'open': '08:00', 'close': '16:30',
    'lunch_close': False, 'lunch_open': False},
  { 'name': "Australia", 'coordinates': [151.2093,-33.8688], 'open': '10:00', 'close': '16:00',
    'lunch_close': False, 'lunch_open': False},
  { 'name': "Shangai", 'coordinates': [121.4737,31.2304], 'open': '09:30', 'close': '15:00',
    'lunch_close': '11:30', 'lunch_open': '13:00'},
  { 'name': "B3", 'coordinates': [-46.6333,-23.5505], 'open': '09:00', 'close': '18:00',
    'lunch_close': False, 'lunch_open': False},
  { 'name': "JSE", 'coordinates': [28.0473,-26.2041], 'open': '09:00', 'close': '17:00',
    'lunch_close': False, 'lunch_open': False},
  { 'name': "TMX", 'coordinates': [-79.3832,43.6532], 'open': '09:30', 'close': '16:00',
    'lunch_close': False, 'lunch_open': False},
  { 'name': "India", 'coordinates': [72.8777,19.0760], 'open': '09:15', 'close': '15:30',
    'lunch_close': False, 'lunch_open': False},
]

for e in exchanges:
    tf = TimezoneFinder()
    tz_string = tf.timezone_at(lng=e['coordinates'][0], lat=e['coordinates'][1])
    tz = pytz.timezone(tz_string)
    time = datetime.datetime.now(tz)

    if time.hour < get_hour(e['open']):
        e['time_left'] = get_time_left(time, get_hour(e['open']), get_minute(e['open']))
        e['state'] = 'close'
    elif e['lunch_close'] is not False and time.hour < get_hour(e['lunch_close']):
        e['time_left'] = get_time_left(time, get_hour(e['open']), get_minute(e['open']))
        e['state'] = 'open'
    elif e['lunch_open'] is not False and time.hour < get_hour(e['lunch_open']):
        e['time_left'] = get_time_left(time, get_hour(e['lunch_open']), get_minute(e['lunch_open']))
        e['state'] = 'close'
    elif time.hour < get_hour(e['close']):
        e['time_left'] = get_time_left(time, get_hour(e['close']), get_minute(e['close']))
        e['state'] = 'open'
    else:
        e['time_left'] = get_time_left(time, get_hour(e['open']), get_minute(e['open']), 1)
        e['state'] = 'close'

return_data = exchanges

print(json.dumps(return_data).replace(' ', ''))
