import sys
import os
import json
from python.db.DB import DB

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))

db = DB()

print(json.dumps(db.get_assessment_heads()).replace(' ', ''))
