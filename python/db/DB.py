import sys
import os
from datetime import datetime
from tinydb import TinyDB, Query
from python.exception.DatabaseException import DatabaseException

sys.path.append('..')


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class DB(metaclass=Singleton):

    def __init__(self):
        self.db = TinyDB(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../data/DATABASE.json'))
        self.q = Query()

    def insert_assessment_head(self, name, end_date=None):
        date = datetime.strftime(datetime.now(), '%Y-%m-%dT%H:%M:%S.%f')
        end_date = end_date if end_date is not None else datetime.now().strftime('%Y-%m-%d')
        self.db.insert({'type': 'assessment', 'name': name, 'date': date, 'head': 'true', 'end_date': end_date})

    def insert_assessment_details(self, name, data):
        date = datetime.strftime(datetime.now(), '%Y-%m-%dT%H:%M:%S.%f')
        if type(data) is list:
            self.db.insert_multiple([{**row, **{'type': 'assessment', 'name': name, 'date': date}} for row in data])
        elif type(data) is tuple:
            self.db.insert(data.update({'name': name, 'date': date}))
        else:
            raise DatabaseException('Bad type given when inserting data in DB : ' + str(type(data)))

    def get_assessment_heads(self):
        return self.db.search((self.q.type == 'assessment') & (self.q.head == 'true'))

    def get_assessment_details(self, name):
        return self.db.search((self.q.type == 'assessment') & (self.q.name == name) & (~self.q.head.exists()))






