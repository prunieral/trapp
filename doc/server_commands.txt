
# trapp application on 8000 port
# launch file on /etc/init/trapp.conf

sudo start trapp
sudo stop trapp

# apache on 80 port

sudo service apache2 start
sudo service apache2 stop
sudo service apache2 restart