# Download and install NodeJS with https://nodejs.org/en/

node -v
npm -v

node server.js

node init

npm install -g express --save
npm install -g grunt-cli --save
npm install -g grunt nodemon --save

grunt

npm install -g react react-dom --save
npm install -g material-ui --save
npm install -g watchify --save
npm install -g babelify --save
npm install -g browserify --save
